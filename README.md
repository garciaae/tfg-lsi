# README
La segmentación de imágenes y en especial la segmentación de imágenes aéreas constituyen un tipo de problemas dentro de la Inteligencia Artificial que aportan gran valor a una variedad de problemas de la vida cotidiana.

En este Trabajo Fin de Grado haremos un estudio práctico de las técnicas existentes para más que obtener un resultado de gran valor añadido, explorar en dichas técnicas y ofrecer un acercamiento de las mismas al lector.

En primer haremos una breve introducción de las partes que componen este trabajo, explicaremos cuál es la motivación del mismo, comentaremos trabajos existentes en este campo y veremos qué es la visión artificial y cuáles son sus orígenes.

Después haremos un recorrido sobre los conceptos relevantes para la comprensión del resto del trabajo con un enfoque _top down_, partiendo de conceptos generales y explicando qué son y cómo están configuradas las redes neuronales.

También haremos un recorrido sobre el _state of art_ o técnicas actuales que permiten resolver el problema mediante técnicas originales o refinamientos de las ya existentes. Definiremos qué es la segmentación y sus tipos cuestionándonos cuáles son sus retos. Asimismo explicaremos cuáles son los bloques en los que se divide un experimento de segmentación y las operaciones que componen las redes neuronales.

Por último haremos una descripción de los distintos experimentos que hemos llevado a cabo y expondremos sus resultados. Haremos un recorrido por una de las plataformas de extracción del conocimiento para este tipo de trabajos.