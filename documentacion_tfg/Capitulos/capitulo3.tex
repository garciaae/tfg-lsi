% !TEX root = ../proyect.tex

\chapter{Segmentación}


\section{Qu\'e es la segmentación}\label{sec:que_es_segmentacion}

Como paso previo a hablar del trabajo relativo a las imágenes aéreas y su segmentación podemos definir la segmentación como el procedimiento por el que una imagen se divide en secciones no superpuestas cuyo elemento común es que los píxeles que la componen pertenecen a una clase discreta. De ese modo podemos determinar con cierto grado de certidumbre qué píxel pertenece a la clase personas, si buscamos personas, árbol, río o campo de cultivo si estamos trabajando con imágenes de terreno, etc.
(Avery Bick, Mullet y Wang, 2018). 

\section{Tipos de segmentación}\label{sec:tipos_de_segmentacion}
En esta sección hablaremos de los distintos tipos de segmentación así como de su arquitectura, componentes y clasificación. Atendiendo a qué buscamos en una imagen, podemos clasificar la segmentación como semántica, de instancias o panóptica.

\begin{definition}[Segmentación semántica]
      En la segmentación semántica haremos una clasificación de píxeles en la que distinguimos entre la pertenencia a la categor\'ia que estamos buscando y la no pertenencia. Si buscamos personas, con la segmentación semántica diremos que tenemos zonas de personas y zonas de no-personas.
\end{definition}

\figura{0.5}{img/cityscapes_semantic_segmentation.jpg}{Segmentación semántica de una escena urbana a pie de calle. Fuente CityScapes}{fig:cityscapes_semantic_segmentation}{}

\begin{definition}[Segmentación de instancias]
      Es el proceso por el que asignamos una etiqueta a cada pixel de la imagen distinguiendo no sólo si una parte de la imagen pertenece a una clase o no sino que distinguiremos entre los distintos elementos pertenecientes a cada clase. Así, si estamos detectando personas, podremos identificar a cada objeto de tipo \textit{persona} individualmente y no solo determinar si pertenece a la clase persona o no.
      La determinación de instancias en una imagen es un paso más allá en el refinamiento de la segmentación de imágenes y es más compleja en términos de computación. Para el tratamiento de imágenes aéreas no lo emplearemos.
\end{definition}

\figura{0.5}{img/instances_segmentation.png}{Segmentación de instancias. Fuente (Ke, Tai, y Tang, 2021)}{fig:instances_segmentation}{}

\begin{definition}[Segmentación panóptica]
      Combina las segmentaciones semántica y de instancias para proporcionar una segmentación más completa. En este tipo de segmentación a cada píxel se le adjunta una etiqueta con la clase a la que pertenece, por ejemplo árbol, y otra con la instancia a la que pertenece, por ejemplo 1, 2, etc.
\end{definition}

\figura{0.5}{img/panoptic_segmentation.png}{Segmentación panóptica. Fuente Facebook Research}{fig:panoptic_segmentation}{}

\section{Los desafíos de la segmentación de imágenes aéreas}\label{sec:aerial_images_challenge}
Desde que apareció la arquitectura U-Net \cite{ronneberger2015unet} la segmentación de \textit{datasets}, especialmente aquellos de imágenes médicas, han experimentado un gran impulso. 

Las imágenes aéreas capturan una gran porción de la tierra a la vez con varias clases, frente a las imágenes médicas que habitualmente pretenden hacer segmentación semántica de un solo tipo.

Los problemas de la segmentación de imágenes aéreas son, por tanto, el tener una menor resolución en las imágenes capturadas y el carecer de distintas perspectivas de las entradas. Esto es debido a la naturaleza de su origen, al estar tomadas estas desde satélites de observación situados a gran distancia, unos 705 kilómetros en el caso del Landsat 8 y de forma cenital.

Otro problema en las segmentación aérea es la ausencia de \textit{datasets} que a la vez tengan gran calidad, estén correctamente etiquetados y tengan gran número de imágenes.

\section{Arquitecturas de los algoritmos de segmentación}\label{sec:arquitecturas_algoritmos}
Desde que se comenzó a trabajar en el problema de la segmentación, ha habido numerosas aproximaciones a la solución del problema hasta que en los últimos años los modelos de \textit{deep learning} han generado toda una nueva generación de algoritmos con resultados que han alcanzado altas tasas de precisión en los \textit{rankings} más populares.

\subsection{Redes neuronales profundas}\label{sec:deep_neural_network_architecture}
\subsubsection{Redes neuronales convolucionales}\label{sec:convolutional_neural_network_architecture}
Las redes neuronales convolucionales, \textit{CNNs}, se encuentran entre las más usadas en la la práctica. Inicialmente basadas en el trabajo de Hubel y Wiesel \citeyear{Hubel1962} fueron propuestas por Fukushima aunque más tarde \cite{21701} introdujeron los pesos compartidos en las capas receptivas.

\figura{0.75}{img/typical_cnn.png}{Estructura típica de una red neuronal convolucional}{fig:typical_cnn}{}

Las CNN suelen estar formadas por 3 capas. La primera capa es una capa de entrada en la que introducimos la imagen o elemento sobre el que queremos trabajar. En segundo lugar solemos encontrar una o varias capas convolucionales de las que se extraen características para formar uno o varios mapas de características. El tercer tipo de capas es el formado por las operaciones de \textit{pooling}, con el que reducimos las dimensiones y aumentamos las conexiones. 

Las unidades básicas están conectadas localmente y su entrada proviene de la capa anterior estando organizadas en forma de pirámide. Todos los campos receptivos de una misma capa comparten los pesos lo que implica menor complejidad que las redes totalmente conectadas. Algunos ejemplos de estas arquitecturas son AlexNet, VGGNet o ResNet y su uso principal suele ser la visión por ordenador.

\subsubsection{Redes neuronales recurrentes}\label{sec:recurrrent_neural_network_architecture}
Este tipo de redes se usan para procesar datos secuenciales o flujos: voz, vídeos, series temporales, etc.

\figura{0.5}{img/rnn_puerta_secuencial.png}{Arquitectura de una RNN}{fig:arquitectura_rnn}{}

Este tipo de arquitectura sufre al abordar problemas en los que las secuencias de entrada son muy grandes dado que no pueden procesar las dependencias a largo plazo.
Para mitigar ese problema, se presenta la arquitectura \textit{Long Short Term Memory}, LTSM en la que se mantiene la memoria durante bastante tiempo, mientras que con las RNN al llegar a cierto tamaño, empezamos a olvidar qué sucedió en el pasado.

Algunas aplicaciones típicas de este tipo de arquitectura son dar la descripción de una imagen, interpretar un lenguaje de programación, traducción de idiomas o predicción de escritura, como las que se usaban en T9, el diccionario de los teléfonos con sistema operativo Symbian.

\subsubsection{Arquitecturas Encoder-Decoder y Auto-Encoders}\label{sec:encoder_decoder_autoencoder_architectures}
Representan una familia de arquitecturas en las que una entrada se transforma en una salida mediante una red de dos etapas: en la primera, el codificador comprime la entrada \textit{x} proporcionando un estado intermedio \textit{z}, que a su vez es la entrada de la segunda etapa, el decodificador, en la que éste predice la salida \textit{y}. Así en la capa intermedia o capa latente, tenemos una representación de la semántica de la entrada que usaremos para predecir la salida.
Este tipo de arquitectura es el núcleo del sistema de traducción de Google.

\figura{0.5}{img/arquitectura_autoencoder.png}{Arquitectura de un autoencoder}{fig:arquitectura_autoencoder}{}

Las aplicaciones de los \textit{autoencoders} son la reducción de ruido y de la dimensionalidad para la visualización de datos, como pueden ser fotografías en mal estado.

\subsubsection{Redes generativas antagónicas}\label{sec:generative_adversarial_network}
Las \textit{Generative Adversarial Networks} o GANs \cite[]{NIPS2014_5ca3e9b1} están formadas por dos redes, la generadora, G, encargada de generar candidatos y la discriminadora, D, que determina qué elementos son reales y cuáles vienen de la red generadora. Esta competencia entre G y D constituye un sistema de suma cero en el que G trata de engañar a D y éste trata de minimizar el error al distinguir entre ejemplos reales y sintéticos.

Las GANs se emplean para generar imágenes foto realistas en renderizados de catálogos, mejora de  modelos astronómicos\footnote{http://3dgan.csail.mit.edu/} y videojuegos.

\subsection{Modelos de segmentación basados en Deep learning}\label{sec:deep_learning_segmentation}
En esta sección recorreremos aquellos modelos basados en \textit{Deep Learning}. Algunos de ellos comparten características de implementación.

\subsubsection{Redes completamente convolucionales}\label{sec:fully_convolutional_networks}
Un \textit{Fully Convolutional Model} o FCN es una arquitectura en la que todas sus capas son operaciones convolucionales y en las que la capa de salida tiene el mismo tamaño que la de entrada, lo que las hace ideales para resolver problemas de segmentación. Sin embargo su complejidad computacional las hace inviables para resolver problemas de segmentación en tiempo real y tampoco funcionan bien en \textit{datasets} de imágenes en 3D.

ParseNet \cite{liu2015parsenet} es una mejora de FCN en la que se mejora la precisión añadiendo una capa con la información contextual que se usa durante las distintas etapas del entrenamiento.

\figura{0.5}{img/parsenet.png}{Mejoras con ParseNet}{fig:parsenet}{}

\subsubsection{Redes convolucionales con modelos gráficos}\label{sec:cnn_with_graphical_models}
Las redes convolucionales funcionan razonablemente bien en problemas de clasificación pero no así en problemas de segmentación. Una aproximación para evitar esto es usar CNN y después otra técnica de campos aleatorios condicionales, \textit{Conditional Random Fields} o CRFs. Estos últimos constituyen un modelo estocástico, compuesto por parámetros aleatorios que varían con el tiempo, que se usa para etiquetar y extraer características de una secuencia de datos. Con esta técnica, el modelo puede encontrar más fácilmente los puntos que delimitan un objeto.

\subsubsection{Modelos DL basados en codificador-decodificador}\label{sec:dl_encoder_decoder_models}
Muchos de los modelos de segmentación basados en DL usan una arquitectura contractiva-expansiva. Atendiendo a su funcionalidad podemos dividir los modelos en aquellos que se usan para segmentar imágenes de carácter general y los que se usan para imágenes médicas.

\subsubsection{Segmentación de imágenes de propósito general}\label{sec:dl_encoder_decoder_models_general}
Basándose en VGG-16, \cite{noh2015learning} propusieron el modelo DeConvNet, que no es otra cosa que tras aplicar un modelo de segmentación, en este caso VGG-16, aplicar capas convolución inversa y de \textit{unpooling} para construir un mapa de segmentación de la imagen inicial. 
\figura{0.5}{img/fcn_large_objects.png}{Etiquetas incorrectas debido al tamaño grande de los objetos en comparación con el resto. De izquierda a derecha imagen original, \textit{ground truth} y resultado de la segmentación}{fig:large_fcn}{}
\figura{0.5}{img/fcn_small_objects.png}{No se etiqueta bien debido al tamaño pequeño de los objetos. De izquierda a derecha imagen original, \textit{ground truth} y resultado de la segmentación }{fig:small_fcn}{}

Típicamente los modelos basados en FCNs se ven afectados bien por objetos muy grandes o por objetos muy pequeños. DeConvNet es capaz de tratar objetos de distintos tamaños dentro de las imágenes, siendo capaz de alcanzar la mejor precisión en el \textit{dataset} PASCAL VOC 2012 en el momento de su publicación.

\subsubsection{Segmentación de imágenes médicas y biológicas}\label{sec:dl_encoder_decoder_models_medical}
Muchos de los modelos inspirados en redes completamente convolucionales y modelos de codificador-decodificador fueron inicialmente desarrollados para la investigación médica. Ahora están siendo usados para otro tipo de propósitos.

\citeA{ronneberger2015unet} propusieron una arquitectura con una rama contractiva y otra expansiva, U-Net, con el objetivo de segmentar imágenes obtenidas con microscopio. 

\figura{0.5}{img/u-net-architecture.png}{Arquitectura U-Net}{fig:arquitectura_u_net}{}

La estrategia de entrenamiento de U-Net reside en el uso de técnicas de \textit{data augmentation} o datos aumentados en las que partiendo de poca cantidad de datos etiquetados para el entrenamiento, podemos aumentar el volumen de los mismos haciendo rotaciones, inviertiendo ejes, o añadiendo ruido a las mismas.

Por otro lado \citeA{milletari2016vnet} proponen V-Net como alternativa a U-Net para resolver el problema de segmentación en imágenes 3D en el que se redefine la función de pérdida y en el que el problema de la falta de datos se soluciona con \textit{Data augmentation}. 

\subsubsection{Redes en pirámide y multiescala}\label{sec:multiscale_pyramid_models}
\textit{Feature Pyramid Network} (FPN) \cite{lin2017feature} diseñada en un principio para la detección de objetos de diferentes tamaños, se puede aplicar también para la segmentación. Al igual que en las anteriores arquitecturas, FPN tiene dos secciones unidas por conexiones laterales.

\figura{0.75}{img/fpn_segmentation.png}{Segmentación con FPN}{fig:fpn_segmentation}{}

En FPN se combinan capas de características de baja resolución y alto contenido semántico con otras de alta resolución y de contenido semántico bajo mediante rutas de refinamiento \textit{top-down} y conexiones laterales.

\subsubsection{Arquitecturas de redes neuronales convolucionales aplicadas en una zona}
Las redes de tipo R-CNN son prácticas a la hora de detectar objetos. Mediante una técnica de detección de fronteras para un objeto de forma rápida, como RPN (\textit{Region Proposal Network}), se extrae la región de interés (ROI) y a partir de estas, se obtienen las características y por último la clase del objeto.

\figura{0.5}{img/mask_rcnn}{Segmentación con Mask R-CNN. Extraemos las zonas de interés con cuadros y aplicamos convolución dentro}{fig:mask_rcnn}{}

En cuanto a la segmentación, R-CNN se puede usar de modo que resuelva ambos problemas simultáneamente: clasificación y detección. 

\figura{0.7}{img/masklab.png}{Imagen original y detección, en marcos grises, junto a la segmentación}{fig:masklab}{}

\subsubsection{Modelos de convolución hueca o dilatada}
La convolución dilatada \cite{yu2016multiscale} --\textit{atrous convolution o algorithme à trous}-- es una operación de convolución con un parámetro adicional que indica la velocidad de dilatación lo que le hace tener un área receptiva mayor que la ordinaria.

\figura{0.7}{img/convolucion_dilatada.png}{Convolución dilatada con \textit{l=1}, \textit{l=2} y \textit{l=3}}{fig:dilated_conv}{}

Los modelos de este tipo han gozado de popularidad en el campo de la segmentación en tiempo real.
\cite{chen2017deeplab} proponen DeepLab2, en la que se hacen tres aportaciones fundamentales. La primera es usar \textit{atrous convolution} para controlar la resolución que se usa como entrada para las \textit{Deep CNN}. En segundo lugar proponen usar \textit{atrous spatial pyramid pooling, ASPP} que permite que en una capa convolucional se usen versiones del objeto a distintas escalas. En tercer lugar, se mejora la detección de los bordes de los objetos usando por un lado \textit{Deep CNNs} como ResNet-111 y por otro métodos probabilísticos gráficos.
\figura{0.5}{img/convolucion_dilatada_en_deeplab2.png}{Convolución dilatada en dos ramas. Arriba, extracción de características con convolución estándar. Abajo convolución dilatada con velocidad de dilatación rate=2}{fig:atrous_convolution}{}

\subsubsection{Modelos basados en redes neuronales recurrentes}
Las \textit{Recurrent Neuronal Networks, RNNs} son prácticas para modelar las relaciones entre elementos contiguos y pueden servir para mejorar la segmentación semántica.

La comprensión de las escenas en tres dimensiones es muy importante en ciertos ámbitos como la robótica o la conducción autónoma. En lugar de reconocer propiedades geométricas o semántica de forma independiente \cite{xiang2017darnn} proponen DA-RNN. En esta arquitectura se trabaja sobre vídeos RGB-D en los que cada cuadro se hace pasar por una RNN para reconstruirse posteriormente junto con las etiquetas.

\figura{0.5}{img/da-rnn.png}{Funcionamiento de DA-RNN. Cada cuadro RGB-D alimenta una RNN. \textit{KinectFusion} reconstruye y enlaza la escena y posteriormente obtenemos la segmentación 3D}{fig:da-rnn}{}

\subsection{Arquitecturas basadas en mecanismos de atención}
La segmentación semántica se ha visto beneficiada por los avances que los sistemas basados en la atención han alcanzado en la visión por ordenador a lo largo de los años.
En \cite{chen2016attention} se establece que una forma habitual de extraer características multiescala es usar la imagen de entrada a distintas resoluciones para extraer características de las mismas y posteriormente usar estas para clasificar a nivel de píxel.

Con \textit{Pyramid Network Attention, PNA}, se hace un camino de \textit{downsampling} mediante ResNet 101 para después aplicar \textit{Feature Pyramid Attention, FPA} y posteriormente emprender el camino de \textit{upsampling} usando cada etapa decodificadora para obtener una mejor interpretación de las características. 
\figura{0.5}{img/pna.png}{Esquema de PNA. Se usa ResNet101 para extraer las características y posteriormente se aplica FPA y GAU para la segmentación}{fig:pna}{}

\subsection{Modelos generativos y entrenamiento antagónico}
Las redes antagónicas se emplean también para la segmentación de imágenes. \cite{luc2016semantic} propusieron, en contraposición a simplemente hacer pasar una imagen por una CNN, entrenar una red convolucional y emplear una red antagónica para discriminar los mapas de características que provienen de la propia red convolucional o de los datos etiquetados o \textit{ground truth}.

\subsection{Modelos convolucionales con contornos activos}
La aplicación de técnicas de redes convolucionales completas y \textit{Active Countour Models, ACM} conforman una combinación de interés.
En \cite{8953484} se aproxima la solución teniendo en cuenta tanto el espacio dentro y fuera de la región de interés como el tamaño de los límites durante el aprendizaje. Se propone una función de pérdida que combina información del área y del tamaño para incorporarla a un modelo de aprendizaje profundo denso.

\figura{0.5}{img/loss_function_acm.png}{Función de pérdida ACM. De izquierda a derecha: imagen propuesta, función de pérdida y segmentación}{fig:acm-loss}{}

\section{Manos en el código}\label{sec:hands_on_code}
Una vez visto el estado del arte para segmentación de imágenes, planteamos el desarrollo.
Partimos de un \textit{dataset}, cualquiera, en el que el tamaño de las imágenes suele ser homogéneo. Esas imágenes suelen estar etiquetadas con capas superpuestas en las que el valor de cada píxel coincidente tiene un valor concreto, por ejemplo y siguiendo con el ejemplo anterior, es persona o no es persona.

Las imágenes, tal y como se ofrecen en los \textit{datasets} no suelen ser manejables, normalmente por el tamaño que ocupan. El primer paso debe ser, por tanto, dividirla en porciones más pequeñas o \textit{chips} que encajen en las unidades de procesamiento que son manejables por el hardware que las trata. Son estas porciones de imagen las que usaremos para entrenar el modelo.

Algunas de las herramientas disponibles para esta primera clasificación son PyTorch, Keras, Caffe, Tensorflow o Theano por citar algunas.

\subsection{Entrenar el modelo}\label{sec:train_the_model}
Entrenar un modelo de datos es un procedimiento conocido. Su objetivo es que el sistema aprenda a llevar a cabo su tarea y usaremos distintos parámetros para conseguirlo.

El método habitual para entrenar un modelo es dividir el conjunto de datos que tenemos etiquetado y usar parte del mismo para el propio entrenamiento y parte para verificar los resultados, es decir, reservaremos un porcentaje pequeño del conjunto de datos de entrada compuesto por imágenes y etiquetas, para comprobar que nuestro modelo funciona adecuadamente. La cantidad de datos que extraeremos puede llegar hasta un 20\%.

\subsection{Operaciones}\label{sec:operaciones}
Los parámetros que podemos usar para influir en cómo el sistema aprende pueden dividirse en función de si perdemos información espacial o la ganamos. Podremos hablar de camino de \textit{down-sampling} cuando perdemos resolución y de \textit{up-sampling} cuando la ganamos.

\subsubsection{Down-sampling}\label{sec:downsampling}
En el camino de bajada, usamos una serie de operaciones que unidas conforman una arquitectura de red neuronal convolucional en la que perdemos resolución a cambio de conocimiento. Algunas de estas operaciones se detallan a continuación.

\textit{Convolución}: operación matemática con dos entradas y una salida. La primera entrada es el objeto 3D correspondiente a la imagen sobre la que operamos. Sus dimensiones son ancho en píxeles, alto en píxeles y canales $n_i$.

La segunda entrada consiste en k filtros o kernels. Cada uno de los filtros tiene dimensiones $f * f *$ el número de canales, con f=3 o f=5.

La salida de una operación de convolución es también una estructura 3D con dimensiones $n_{out} * n_{out}$ * número de canales pudiendo calcular $n_{out}$ del siguiente modo:
\[n_{out} = \left\lfloor{\frac{n_{in} + 2p - k }{ s }}\right\rfloor{} + 1 \]
\begin{itemize}
    \item $n_{out}$: número de características de salida
    \item $n_{in}$: número de características de entrada
    \item k: tamaño del kernel o filtro
    \item p: tamaño del \textit{padding} o relleno. El \textit{padding} es un marco alrededor de los datos de entrada para asegurar que el conjunto inicial se procesa por completo.
    \item s: tamaño del \textit{stride} o paso. Controla cómo el filtro avanza sobre los datos de entrada y determina el tamaño del dato de salida
   \end{itemize}

Algo realmente importante que debemos tener en cuenta es que la operación de convolución establece una conexión entre los valores de entrada y salida. Las operaciones sobre la parte superior de la entrada, afectarán a la parte superior de la salida, las de la parte izquierda a la parte izquierda y así sucesivamente. Realmente establecemos una relación n a 1 sobre los elementos de la matriz de salida.

\textit{Max-pooling}: reduce el tamaño de la capa de datos para tener menos datos en la red. Al igual que con la operación de convolución, usamos filtros y \textit{stride} o paso.

Esta operación elige el valor máximo de los que componen el filtro en cada momento para conformar un mapa. Reduce la complejidad a cambio de perder información. Con el \textit{pooling} conseguimos que el modelo comprenda qué hay en la imagen a cambio de perder dónde está presente. Eso es un problema en cuanto a que estamos resolviendo un problema de segmentación, en el que hay que determinar qué hay en la imagen etiquetando todos sus píxeles.
Por tanto, para el problema de segmentación necesitamos bajar haciendo down-sampling para después subir haciendo up-sampling.

\subsubsection{Up-sampling}\label{sec:upsampling}

De-convolución o convolución traspuesta: es la técnica opuesta a la convolución en la que el objetivo es que usando como entrada la matriz de salida de una operación de convolución, llegar a una aproximación del conjunto de datos inicial.

Para hacer up-sampling de una imagen podemos elegir técnicas de interpolación habituales, como interpolación bilineal o bicúbica o bien usar métodos en los que la red aprenderá cómo hacer up-sampling óptimamente.

La ventaja de hacer convolución traspuesta frente a técnicas tradicionales de interpolación es que puedes usar diferentes \textit{kernels} y usarlos para hacer la convolución traspuesta en aras de conseguir de nuevo algo muy parecido a nuestros datos originales.

\subsection{Uniendo las piezas: elegir una arquitectura}\label{sec:choose-architecture}
Podemos elegir diferentes arquitecturas: LeNet, AlexNet, VGG, GoogLeNet, Inception V3, Inception BN, o U-Net.

Pese a que U-Net ha sido superada por otras arquitecturas, hemos decidido usarla por varios motivos: supuso un cambio importante en lo que había hasta el momento, es sencilla de implementar y fácil de entender. Es por eso que hemos creado una implementación de la misma desde cero, que hemos incluido en el repositorio de código de GitLab\footnote{https://gitlab.com/garciaae/tfg-lsi/}. Para los experimentos de la sección \ref{sec:results} hemos usado sin embargo la versión proporcionada por la librería Keras.

\subsubsection{Arquitectura U-Net}\label{sec:unet-architecture}
La arquitectura U-Net recibe este nombre porque su representación esquemática tiene forma de U. Es una arquitectura en la que tenemos una rama contractiva o de \textit{downsampling} y otra rama simétrica expansiva o de \textit{upsampling}. Está diseñada para la segmentación de imágenes, es rápida, sencilla y hay mucha información disponible.

Una de sus características originales es la de que en la rama expansiva o decodificadora, las entradas no sólo están compuestas por la salida de la capa anterior sino que están alimentadas por la salida de la capa contractiva de la misma altura. Así recuperamos la información de la localización de la información que hemos ido encontrando.

El primer paso es convertir la información de las imágenes de entrada, cuyos valores se encuentran en el rango de 0 a 255 a valores en punto flotante entre 0 y 1 por conveniencia para el resto de operaciones, que trabajan en ese rango.

Después empezamos con las capas de extracción de características de la rama contractiva. Vamos aplicando operaciones de convolución, \textit{dropout} y \textit{pooling} con distintos parámetros como la función de activación, el \textit{inicializador} del kernel y el \textit{padding}.

La función de activación es usada en la capa para devolver un valor a partir de una entrada. Buscamos una función sencilla en términos de computación cuya salida esté en el rango (-1, 1) o (0, 1). ReLu o \textit{Rectified Lineal Unit} cumple esas premisas y devuelve 0 si la entrada es negativa y el valor de la entrada si ésta es positiva. Es una función que empíricamente tiene un buen comportamiento para el tratamiento de imágenes usando deep learning.

El inicializador del kernel da valores iniciales a éste. Hemos usado \textit{HeNormal} que actualiza los pesos iniciales en base a una distribución normal, centrada en 0 y truncada.

Por último, el parámetro \textit{padding} con valor \textit{same} hace que el tamaño de la salida de la capa sea el mismo que el de la entrada.

En la capa contractiva, iremos aplicando de forma recurrente capas de convolución, para extraer características, de dropout, que nos permiten descartar datos para no sobre-entrenar y pooling, que reducen el tamaño de la capa de datos.

Partimos de una capa y al final del primer paso, tras el tren de operaciones, tendremos 64. Cada flecha azul hacia la derecha supone una operación de convolución, \verb|Conv2D| en el código expuesto. Cada flecha roja hacia abajo, una operación de \textit{MaxPooling}, \verb|MaxPooling2D|, con la que dividimos la imagen a la mitad.

\figura{0.25}{img/unet_step1.png}{Primer paso de la rama contractiva}{fig:unet_step1}{}
\begin{lstlisting}[language=Python, caption=Código del primer paso de la rama contractiva usando un dropout del 10\%]
c1 = tf.keras.layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(s)
c1 = tf.keras.layers.Dropout(rate=10/100)(c1)  # Dropout de un 10% para no sobreentrenar
c1 = tf.keras.layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
p1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c1)
\end{lstlisting}

\figura{0.5}{img/unet_step2.png}{Segundo, tercer y cuarto pasos de la rama contractiva. Repetimos las operaciones sucesivamente}{fig:unet_step2}{}
\newpage
\begin{lstlisting}[language=Python, caption=Código de los pasos de rama contractiva]
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p1)
c2 = tf.keras.layers.Dropout(0.1)(c2)
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p2)
c3 = tf.keras.layers.Dropout(0.1)(c3)
c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p3)
c4 = tf.keras.layers.Dropout(0.2)(c4)
c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
p4 = tf.keras.layers.MaxPooling2D((2, 2))(c4)

c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p4)
c5 = tf.keras.layers.Dropout(0.3)(c5)
c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)
\end{lstlisting}

Una vez alcanzada la base, tendremos una imagen de 28 píxeles de lado con una profundidad de 1024 capas.
\figura{0.4}{img/unet_step3.png}{Base de la rama contractiva. Haremos dos operaciones de convolución pero no haremos \textit{MaxPooling}}{fig:unet_step3}{}

En la rama expansiva, nuestro objetivo es alcanzar el tamaño inicial de la imagen. Para ello usaremos una operación que se llama convolución transpuesta \verb|Conv2DTranspose| en la que sucesivamente vamos aumentando las dimensiones de la imagen disminuyendo su profundidad.

En el primer paso de esta rama partimos de la imagen anterior, de 28 x 28 x 1024 para mediante una operación de convolución inversa, llegar a una imagen de 56 x 56 x 512. En este momento, combinamos la imagen del paso contractivo correspondiente para tener una salida de 56 x 56 x 1024 en la que la imagen crece pero conservamos el número de capas.

\figura{0.4}{img/unet_step4.png}{Rama expansiva. La flecha gris indica la concatenación de la imagen del paso de la misma profundidad en la rama contractiva}{fig:unet_step4}{}

\begin{lstlisting}[language=Python, caption=Código de los pasos de rama expansiva o \textit{upsampling}]
# Rama expansiva o decoder
u6 = tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=(2, 2), strides=(2, 2), padding='same',)(c5)
u6 = tf.keras.layers.concatenate(inputs=[u6, c4])
c6 = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(rate=20/100)(c6)
c6 = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)
\end{lstlisting}

Al igual que en la rama contractiva, vamos repitiendo los pasos pero en sentido inverso hasta que alcanzamos la parte más alta posible, a la altura del paso inicial y en la que tendremos que cambiar las dimensiones de la imagen para obtener el mapa de la segementación que estábamos buscando.

\figura{0.125}{img/unet_step5.png}{Rama expansiva: cambio de resolución de la imagen para obtener el mapa de la segmentación}{fig:unet_step5}{}

Con U-Net se consiguen buenos resultados incluso teniendo pocos datos para el entrenamiento. Se comporta bien con técnicas de aumento de datos (mediante rotaciones, traslaciones, giros y reflexiones) lo cual la hace ideal para segmentar \textit{datasets} de imágenes aéreas en las que las imágenes son escasas y difíciles de conseguir.