% !TEX root = ../proyect.tex

\chapter{Diseño de la solución}

A lo largo de este capítulo evaluaremos el sistema en el que hemos trabajado para comprobar de manera experimental la segmentación de imágenes aéreas.

Empezaremos con la descripción del \textit{dataset} o conjunto de datos empleado. Seguiremos con el \textit{software} que hemos empleado tanto para desarrollar la solución como para recopilar los resultados de la misma, agruparlos y compararlos. Y por último, mostraremos los resultados obtenidos.

\section{Arquitectura de la solución}\label{sec:arquitectura_solucion}

\subsection{Elección del conjunto de datos}\label{sec:choose_dataset}
Para la realización de la parte práctica proyecto hemos utilizado el \textit{dataset} de la compañía DroneDeploy. Este \textit{dataset} fue liberado como parte de un concurso en el que se trataba de encontrar, usando técnicas ya desarrolladas, la configuración que obtuviese una mejor métrica. Para establecer la clasificación, los organizadores del concurso utilizaron el parámetro f1\_mean, en el que se combinaban la precisión y la exhaustividad o \textit{recall} de modo que cuánto mayor fuese el parámetro mejor sería la segmentación.

El conjunto de imágenes está dividido en tres partes. Por un lado tenemos imágenes con información de elevación, la correspondiente imagen real formada por orto-mosaicos aéreos y por último la imagen con la información contextual. 

Se ofrece el \textit{dataset} en dos tamaños, el pequeño, formado por 3 imágenes y el completo, compuesto por 38 con un tamaño de 8555 MB.

Las imágenes se distribuyen en formato TIFF, sin compresión en la que cada píxel tiene 3 canales, azul, verde y rojo con tamaños que oscilan entre los 8.8 MB de la más pequeña y los 667 MB de la mayor. La resolución de las imágenes es de 10 centímetros por píxel lo cual puede ser considerado de muy alto nivel de detalle según la clasificación vista en las secciones \ref{sec:segmentation_information} y \ref{sec:aerial_images_challenge} y nos proporcionarán, una vez dividida en sub-imágenes más pequeñas o \textit{chips}, la base para efectuar el ajuste, la validación y las predicciones.

Para las elevaciones se ofrecen las imágenes que concuerdan con las correspondientes orto-fotos pero con un solo canal en las que el valor en punto flotante corresponde con la correspondiente altura de la imagen asociada. En este trabajo no hemos usado dicha información para mejorar la eficiencia en la segmentación y lo propondremos como una posible mejora del mismo.

\figura{0.75}{img/labels.png}{Etiquetas proporcionadas por el \textit{dataset} de DroneDeploy superpuestas sobre una imagen.}{fig:drone-deploy-labels}{}

Por último, las etiquetas asociadas a cada imagen están divididas en 7 clases distintas: suelo, agua, vegetación, vehículos, desorden/escombros, edificios y zonas a ignorar (cuadro \ref{tab:clases_dronedeploy} e imagen \ref{fig:drone-deploy-labels}). Nos servirán para entrenar al modelo elegido. Esta información se distribuye en forma de imágenes en formato \verb|PNG| del mismo tamaño que las imágenes de entrada. Al entrenar el modelo, cualquier \textit{chip} o cuadro que contenga información de esta última categoría, \textit{ignorar}, será excluido del entrenamiento por simplicidad.

\cuadro{|l|l|r|r|}{Distribución de las clases en el \textit{dataset} de DroneDeploy.}{tab:clases_dronedeploy}
{
    Clase & Color & Índice & Porcentaje del total \\
    \hline
    Ignorar & Magenta & 0 & 42.7\% \\ 
    Edificios & Rojo & 1 & 5.6\% \\
    Escombros & Morado & 2 & 2.0\% \\
    Vegetación & Verde & 3 & 10.4\% \\
    Agua & Naranja & 4 & 1.2\% \\
    Suelo & Blanco & 5 & 37.7\% \\
    Vehículos & Azul & 6 & 37.7\% \\
}

\subsection{Análisis de las herramientas}\label{sec:tooling}

Tensorflow \cite{abadi2016tensorflow} es el software \textit{open source} mediante el cual podemos describir algoritmos de \textit{machine learning} y la implementación para poder ejecutar dichos algoritmos. Un sistema descrito mediante Tensorflow puede ser ejecutado sin variaciones en distintos sistemas construidos de forma heterogénea y con distintas arquitecturas tales como un teléfono móvil, un ordenador doméstico o un servidor con varias GPUs.
Hemos elegido Tensorflow/Keras frente a otras alternativas como Pytorch por ser un \textit{framework} de más alto nivel, en el que se puede hacer más con menos instrucciones de código pese a que PyTorch permite mayor control del detalle. Otro motivo ha sido el haber encontrado más documentación y mejor estructurada. Estos motivos, cuando hay fechas de entrega que cumplir, son importantes puesto que el equipo de desarrollo es más productivo en términos de hitos por unidad de tiempo.

En cuanto al hardware, hemos usado dos dispositivos: un ordenador portátil MacBook Pro con 16 GB de RAM y una CPU Apple M1 para la programación del software y depuración de los modelos y el servidor Deepknowledge con una CPU Intel Core i7-6700K CPU a una frecuencia de 4.00GHz 64 GB de RAM con dos GPU. Por un lado una tarjeta gráfica GeForce GTX 1070 con 8 GB de RAM tipo DDR5 y por otro una TITAN Xp con 12 GB de RAM.

En el lado de la infraestructura, hemos usado el servidor Deepknowledge ubicado en la Escuela Técnica Superior de Ingeniería Informática, en el que hemos hecho la mayoría de pruebas. Por otro, hemos adaptado el código para correr en un Jupyter notebook \cite{soton403913} en Google Colab. Google Colaboratory es la plataforma Cloud de Google que permite el uso gratuito de GPU y TPUs. Pese a tener algunas limitaciones, como un tiempo máximo de ejecución por sesión de 12 horas y un tiempo sin interacción del usuario de 1.5 horas éste servía perfectamente para el tipo de experimento que queríamos hacer.

\subsection{Elección de la plataforma para la presentación de resultados}\label{sec:results_framework}

WandB es el acrónimo para \textit{Weights and biases} \cite{wandb} y es una plataforma diseñada para rastrear, visualizar y comparar experimentos de \textit{machine learning} de forma agnóstica, esto es, sin depender de un determinado \textit{framework}. Es gratuito para proyectos personales e instituciones y dota de gran control sobre los proyectos permitiendo visualizar de forma sencilla los resultados obtenidos. Asimismo permite una integración sin esfuerzo de los proyectos existentes mediante la inclusión de unas pocas líneas de código en los mismos.

\figura{0.75}{img/wandb_ui.png}{Interfaz gráfico de WandB}{fig:wandb_ui}{}

En un proyecto software tradicional destinado a producción, nadie se atrevería a desarrollar sin un sistema de control de versiones. Sin embargo, muchos de los investigadores en ML usan versiones a medida para almacenar las métricas y presentar sus resultados. La tendencia en este campo es la de publicar el código junto a un \textit{paper}, lo cual parece insuficiente en términos de comparación de resultados.
La consecuencia de no usar control de versiones provoca numerosos problemas: transferencia de conocimiento, dificultad para depurar errores debido a no tener captura de \textit{logs} etc.
Asimismo es difícil capturar los errores para conjuntos de datos que difieren entre las versiones de producción y de entrenamiento debido al hecho de no capturar los resultados del entrenamiento.

Con WandB se pueden construir experimentos de forma más rápida y posteriormente visualizar todos los elementos que componen una sesión de \textit{ML}.
Las entradas de WandB no son solo el código que generó el modelo y los datos. También se pueden guardar los hiperparámetros junto con los datos de entrenamiento. Idealmente se deberían guardar las librerías instaladas y las variables de entorno para tener un entorno reproducible.

Otra de las características que hacen interesante a WandB es lo posibilidad de almacenar todos los resultados intermedios de la ejecución de un proyecto software: \textit{logs} del terminal, estadísticas del sistema, archivos intermedios, modelos, etc.

WandB permite también la colaboración en tiempo real entre distintos miembros de un equipo para explicar cómo funciona el modelo, mostrar gráficos de las distintas versiones del mismo, discutir errores y exponer el mismo.

Integrar WandB en un proyecto existente es tan sencillo como importar una librería con \textit{Pypi} y añadir unas pocas líneas de código que lanzan un proyecto en segundo plano cuyos recursos no afectan de forma significativa a la ejecución del proyecto.

Sin duda, una de las características más notable de WandB es la posibilidad de comparar distintas ejecuciones de un proyecto con ligeros cambios para poder encontrar los parámetros óptimos. Profundizaremos más tarde en este aspecto.

\section{Descripción del sistema y resultados}\label{sec:results}
En esta sección haremos una descripción más detallada del sistema, y explicaremos cómo se ha procedido y qué experimentos hemos llevado a cabo para desarrollar los contenidos de este trabajo.

\subsection{Bifurcando el código}\label{sec:getting:the_code}

Una vez determinado el conjunto de datos, el siguiente paso natural ha de ser entrenarlo. Para ello, DroneDeploy\footnote{https://github.com/dronedeploy/dd-ml-segmentation-benchmark} pone a disposición pública un \textit{script} Python con en el que tenemos un punto de partida para poder trabajar con los datos, entrenar el modelo, validarlo y hacer algunas predicciones. 

El primer paso que se siguió fue hacer un \textit{fork} del proyecto original. Un \textit{fork} o bifurcación es una escisión del código de un proyecto que suele hacerse bien para colaborar con un proyecto en el que no somos desarrolladores oficiales o bien para usarlo de punto de partida en de un proyecto nuevo. Para alojarlo se optó por GitLab. 

El hacer \textit{fork} de un proyecto tiene otras implicaciones para este trabajo. Entre ellas está que pese a ser ejecuciones del mismo código pero, por el hecho de ser un \textit{fork}, los resultados de las ejecuciones desde esta máquina se almacenan en un proyecto distinto en WandB. Así, tendremos por un lado las ejecuciones del proyecto \textit{vanilla} o sin modificaciones y por otro las de nuestro \textit{fork}.

\subsection{Haciendo funcionar el código}\label{sec:making_the_code_work}

El primer problema que se encontró fue que dicho código está liberado como un proyecto PyPi cuyo archivo de requisitos ni incluía las versiones de los paquetes usados ni estaba completo. Lo ideal en un proyecto PyPi es proporcionar la suficiente información sobre su construcción para poder replicarlo en cualquier entorno. En este caso, al ser una mera prueba de concepto hemos encontrado que para los distintos módulos requeridos por el mismo tan solo se estaba proporcionado el nombre del paquete. Lo habitual es proporcionar también la versión. De este modo, en el momento de ser publicado, éste habrá podido ser replicado sin problemas junto con las versiones contemporáneas. Conforme ha pasado el tiempo ha dejado de hacerlo.

Por tanto, la clave para ejecutar el proyecto estaba en buscar los paquetes de Python más próximos a la fecha en la que se actualizó por última vez el repositorio. Para ello se consultó el archivo de cambios de cada uno de los paquetes en el sitio web de PyPi. El listado de paquetes y versiones ha quedado según se refleja en el cuadro \ref{tab:requirements_txt}

\cuadro{|l|c|}{Versiones de los paquetes utilizados con el repositorio de DroneDeploy}{tab:requirements_txt}
{
    Paquete Python & Versión \\
    \hline
    fastai & 2.1.2 \\
    h5py & 2.10.0 \\
    image-classifiers & 1.0.0 \\
    ipdb & 0.13.4 \\
    keras & 2.4.3 \\
    Keras-Applications & 1.0.8 \\
    Keras-Preprocessing & 1.1.2 \\
    numpy & 1.17.3 \\
    opencv-python & 3.4.8.29 \\
    prompt-toolkit & 2.0.10 \\
    sklearn & 0.0 \\
    tensorflow & 2.3.1 \\
    torch & 1.7.0 \\
    typing & 3.6.6 \\
    wandb & 0.8.13 \\
}

En cuanto a la versión de Python, al estar trabajando sobre un servidor remoto en el que no tenemos permisos de administración, sólo podían usar las versiones nativas que estuviesen instaladas en dicho servidor. Dado que el servidor proporcionaba las versiones 2.7, 3.5 y 3.6 decidimos habilitar un entorno virtual Python e instalar en él todos los paquetes necesarios junto con una copia de Python 3.6.

\subsection{Análisis del rendimiento en el modelo presentado}\label{sec:base_performance}
Antes de profundizar en un entrenamiento con el conjunto completo de datos, haremos pruebas con los distintos parámetros y exploraremos diferentes opciones en términos de codificadores, aumento, normalización y funciones de pérdida.

El ejemplo propuesto en el repositorio GitHub del conjunto de datos DroneDeploy utiliza la arquitectura
arquitectura U-Net, con implementaciones usando Keras o FastAI.

\subsubsection{Métricas usadas en los experimentos}\label{sec:running_metrics}
En el cuadro \ref{tab:metrics} presentamos una lista de las métricas que hemos usado a lo largo de los experimentos junto con una breve explicación de las mismas.
\cuadro{|l|l|}{Métricas y parámetros usados en los experimentos}{tab:metrics}
{
    Nombre & Descripción \\
    \hline
    f1\_mean & Media de f1 o \textit{Dice Score} que sintetiza las medidas de \\
    & recall y precisión en un valor único. \\
    \hline
    Precision & Indica la calidad del modelo. \\
    \hline
    pr\_mean & Media de los valores de precisión. \\
    \hline
    Recall & Exahustividad. Mide la cantidad de datos que el modelo reconoce. \\
    \hline
    re\_mean & Media de los valores de exhaustividad. \\
    \hline
    Loss & Valor de la función de pérdida. \\
    \hline
    f\_beta & Similar a f1. Combina los valores de precisión y recall \\
    & dando mayor peso a la exahustividad. \\
    \hline
    Sweep & Nombre del experimento agrupado. \\
    \hline
    bs & Tamaño del bloque de procesamiento. \\
    \hline
    lr & learning rate o tasa de aprendizaje. \\
    \hline
    size & Tamaño de la sub-imagen o \textit{chip}. \\
    \hline
    epoch & Iteración en el entrenamiento de un modelo. \\
    \hline
    optimizer & Genera pesos para minimizar el error. Hemos usado ADAM \\
    & y SGD en los experimentos. \\
    \hline
    momentum & Es un parámetro de entrada para el optimizador. \\
    \hline
    GPU Count & Número de GPUs en el entrenamiento. \\
    \hline
    mIOU & Media de la métrica \textit{Intersection Over Union} muy usada en \\
    & segmentación. \\
    \hline
}

\subsubsection{Experimento base. Ejecución sin modificaciones}\label{sec:no_changes_run}
Primero establecemos una línea de base con esta implementación y obtenemos puntuaciones \textit{f1\_mean} de 0.7779 y 0.7447 utilizando Keras y FastAI respectivamente, que se entrenan durante 15 \textit{epochs}. Los resultados de esta ejecución se pueden ver en el cuadro \ref{tab:vanilla_run} y el único interés que tienen es el de comparar ambas librerías, Keras y FastAI. Podemos ver que ambas implementaciones se comportan de forma similar y no se puede establecer una preferencia de una frente a la otra.

\figura{0.65}{img/out_of_the_box_segmentation.png}{Segmentación sin cambios usando el \textit{encoder} ResNet50. De izquierda a derecha, imagen original, resultado de la segmentación y \textit{ground truth}.}{fig:out-of-the-box-segmentation}{}

\cuadro{|l|l|c|c|c|}{Ejecución del código \textit{out of the box} con el dataset simple.}{tab:vanilla_run}
{
    \textit{Library} & \textit{Backbone} & f1\_mean & pr\_mean & re\_mean \\
    \hline
    Keras & ResNet18 & 0.7779 & 0.7557 & 0.8039 \\
    FastAI & ResNet18 & 0.7447 & 0.7186 & 0.8009 \\
}

\subsubsection{Variaciones en los \textit{encoders}}\label{sec:play_with_encoders}
El siguiente paso natural es usar distintos codificadores o \textit{encoders}, jugar con las opciones para la función de pérdida, aumento del volumen de datos \textit{data augmentation} e incluso podemos cambiar el tamaño del mosaico o \textit{chip} y ver de qué modo afecta al rendimiento de la segmentación.

Para llevar un proceso metódico hemos cambiado un parámetro cada vez, empezando por los \textit{encoders}. Los datos del primer set de experimentos fueron usar el \textit{dataset} simple, durante 15 \textit{epochs} cambiando los codificadores y con un tamaño de \textit{chip} de 300 puntos de lado con forma cuadrada:
\cuadro{|l|lllllll|}{Ejecuciones del código con los distintos \textit{encoders}. La descripción de las métricas puede consultarse en \ref{tab:baseline_medium_vgg19}.}{tab:baseline}
{
    \hline
    Name      & f\_beta & f1\_mean & pr\_mean & re\_mean & precision & recall & \textit{loss} \\
    \hline
    ResNet18  & 0,8930  & 0,7700   & 0,7529   & 0,7976   & 0,9030    & 0,8832 & 0,3200 \\
    VGG16     & 0,7495  & 0,0815   & 0,4469   & 0,0831   & 0,7578    & 0,7413 & 0,7168 \\
    VGG19     & 0,7354  & 0,1030   & 0,4560   & 0,0894   & 0,7435    & 0,7275 & 0,7525 \\
    ResNet101 & 0,6621  & 0,6410   & 0,7025   & 0,6234   & 0,6694    & 0,6549 & 1,0817 \\
    ResNet152 & 0,6310  & 0,2312   & 0,5597   & 0,1935   & 0,6379    & 0,6243 & 1,0978 \\
    ResNet50  & 0,6309  & 0,4450   & 0,6609   & 0,3811   & 0,6379    & 0,6241 & 1,1014 \\
    \hline
}

\figura{0.75}{img/different_encoders.png}{Evolución de \textit{f\_beta} respecto al número de epochs usando diferentes \textit{encoders} y tipos de procesador.}{fig:different_encoders}{}

En primer lugar, hay que hacer notar que este experimento en concreto no ofrece la mejor información debido a que el \textit{dataset} es pequeño aunque las imágenes son grandes y el número de \textit{chips} también lo es. Hemos ordenado el conjunto de ejecuciones por el parámetro \textit{f\_beta}, un índice que incluye la precisión y la exhaustividad o \textit{recall} en su cálculo y que explica que haya correlación entre ambos de modo que para valores más altos de \textit{f\_beta} tendremos mejor precisión y \textit{recall}. El \textit{encoder} que mejor se comporta en este conjunto de ejecuciones es ResNet18.

\subsubsection{Variaciones en los \textit{epochs}}\label{sec:play_with_epochs}
Un experimento algo más interesante tiene que ver con el número de \textit{epochs} o iteraciones. En este caso compararemos tres ejecuciones en las que la solamente variaremos el número de \textit{epochs} que ejecutamos. Pese a que la segunda ejecución, B, hubo de ser interrumpida por razones de mantenimiento del servidor dónde se ejecutaba, ofrece unos datos lo suficientemente buenos para incluirla entre los datos recopilados. La salida de estos experimentos se puede ver en el cuadro \ref{tab:baseline_medium_vgg19}.

Para ejecutar este proyecto hemos elegido los siguientes hiperparámetros de configuración. El \textit{encoder} ha sido en todos los casos VGG-19 \cite{simonyan2015deep}, compuesto por 16 capas convolucionales y 3 capas \textit{fully connected}, con un \textit{learning rate }de 0.001, un tamaño de chip de 300 sobre el \textit{dataset} completo de DroneDeploy. Hemos elegido VGG-19 porque ofrece resultados suficientemente buenos con un tiempo de ejecución ajustado. También hemos usado el optimizador ADAM y para la función de pérdida hemos usado \textit{categorical cross entropy loss} o función de pérdida para categorías de entropía cruzada, indicada para la salida de experimentos en los que hacemos una clasificación.

En este experimento apreciamos que si bien los trabajos estaban programados para iterar durante 30 \textit{epochs}, alcanzaron sus mejores métricas antes. En concreto, la primera ejecución (A) la alcanzó en el \textit{epoch} 23. La segunda (B) en el \textit{epoch} 14 y la última (C) entre el primero y el séptimo epochs. Sus métricas se pueden ver en el cuadro \ref{tab:baseline_medium_vgg19}.

\figura{0.95}{img/vgg-19_30_epochs_medium.png}{Imagen, predicción, \textit{ground-truth} y \textit{overlay} al 55\% de transparencia de la ejecución del proyecto usando el \textit{encoder} VGG-19 durante 30 epochs sobre el \textit{dataset} extendido. Ejecución A del cuadro \ref{tab:baseline_medium_vgg19}.}{fig:prediction_vgg-19}{}

 La explicación para ello es que el entrenamiento va alcanzando cierta madurez y a partir de un punto determinado del mismo, las variaciones son mínimas. Al haber elegido una estrategia de optimización que nos permite acortar los tiempos dedicados al entrenamiento,en nuestro caso la función \textit{EarlyStopping}, ésta se invoca parar detener el proceso global. De este modo conseguimos no llegar hasta el final de un proceso ya que no se van a obtener resultados mejores que los que ya hemos alcanzado.

En este caso hemos configurado \textit{EarlyStopping} para que observe la métrica \textit{val\_loss} con un parámetro de \textit{patience} o paciencia de 3 \textit{epochs} lo que significa que si en 3 iteraciones consecutivas el valor de dicho parámetro no ha mejorado detendremos la ejecución.

\cuadro{|l|lllllllcl|}{Ejecuciones del código usando el \textit{encoder} VGG-19, y el dataset extendido con los parámetros \textit{learning rate}: 0.001, \textit{chip size}: 300 y optimizador ADAM}{tab:baseline_medium_vgg19}
{
    \hline
    Name  & f\_beta & f1\_mean & pr\_mean & re\_mean & precision & recall & loss & epochs & best \\
    \hline
    A     & 0,9332  & 0,8048   & 0,8400   & 0,7884   & 0,9380    & 0,9283 & 0,1820 & 30 & 23 \\
    B     & 0,9248  &   -      &   -      &   -      & 0,9296    & 0,9200 & 0,2098 & 22 & 14 \\
    C     & 0,8955  & 0,7177   & 0,8194   & 0,6754   & 0,9002    & 0,8909 & 0,3090 & 7 & - \\
    \hline
}

En conclusión, podemos afirmar que el entrenamiento sobre el \textit{dataset} completo, con las suficientes iteraciones ofrece unos resultados satisfactorios en la predicción, llegando a alcanzar el 86\% en el reconocimiento de vegetación y el 80\% en suelo para el ejemplo A del cuadro \ref{tab:baseline_medium_vgg19}. Hemos de resaltar que la métrica f1\_mean obtiene un resultado mejor que la ejecución inicial debido fundamentalmente al tamaño del conjunto de datos. Por contra, el experimento A se mostró más débil en el reconocimiento de la categoría \textit{clutter} o escombros con tan solo un 32\% de reconocimiento según aparece en la imagen \ref{fig:confusion_vgg-19}

\figura{0.65}{img/vgg-19_30_epochs_medium_confusion.png}{Matriz de confusión de las categorías detectadas para una de las imágenes usando el \textit{encoder} VGG-19 correspondientes al experimento A.}{fig:confusion_vgg-19}{}


\subsection{Barrido de hiperparámetros}\label{sec:hyperparameters_sweep}
Otro experimento digno de mención consiste en usar la optimización de hiperparámetros de WandB o \textit{hyperparameters sweeps}. WandB proporciona esta útil herramienta en la que se hace un recorrido automatizado de los distintos hiperparámetros, como pueden ser \textit{epochs}, \textit{learning rate}, optimizadores), y mediante la cual podremos descubrir ciertas combinaciones prometedoras dentro del espacio multidimensional que configuran un experimento de \textit{ML}. 

Con \textit{WandB sweeps} se consiguen combinaciones de parámetros que ofrecen mejores resultados que los conseguidos por intuición.
Su configuración se hace de forma sencilla. Simplemente tendremos que añadir varias líneas de código al proyecto existente y estaremos listos para trabajar con ella.

\figura{0.5}{img/central_sweep_server.png}{Esquema funcional de WandB \textit{sweeps}}{fig:central_sweep_server}{}

Para usar esta característica, en primer lugar tendremos que tener importar la librería \textit{WandB} a nuestro proyecto. Esta librería está disponible en el repositorio público de PyPi. Después escribiremos un fichero de configuración con formato \verb|YAML| en el que especificaremos qué hiperparámetros queremos optimizar, elegir una estrategia de optimización y otra de iteración. En nuestro caso hemos elegido \textit{Early Stopping} para esta última.

Una vez tenemos la configuración tendremos que preparar el barrido de parámetros mediante un comando gracias al cual damos valores iniciales. 

Por último ejecutamos el agente. El agente se encarga por un lado de elegir de entre los parámetros propuestos en el fichero \verb|YAML|, aquellos que usaremos en una ejecución y por otro, hacer que las métricas, resultados y registros que se ejecuten en la máquina local queden registrados y agrupados en nuestra cuenta de WandB. Podemos ejecutar distintos agentes en distintas máquinas que permiten escalar con esta solución como vemos en la Imagen \ref{fig:central_sweep_server}. Indicaremos en cada uno de los experimentos que vienen a continuación qué parámetros son los que el agente puede ir variando.

\subsubsection{Experimentos con barridos de parámetros}\label{sec:sweep_experiments}
Algunos experimentos que podemos hacer nos permiten modificar un solo parámetro como los \textit{epochs}. Esto puede ser útil para encontrar el punto óptimo en el cual más entrenamiento no significa mejores resultados.

Donde encontramos más versatilidad para esta herramienta es en la elección de la estrategia de selección de parámetros. Para ello disponemos de tres métodos distintos que son \textit{grid}, aleatorio y Bayesiano. 

La estrategia \textit{grid} consiste en recorrer cada uno de los parámetros propuestos de forma exhaustiva. Este método es bastante completo pues recorre todas las posibles combinaciones de parámetros. Cuando el número de éstos es muy alto, las combinaciones se hacen inmanejables en términos de tiempos de computación.

En el polo opuesto tenemos la estrategia aleatoria. Con ella se eligen los parámetros al azar pudiendo llegar a resultados buenos pero sin ninguna garantía de éxito.

Por último tenemos la estrategia Bayesiana. En ésta estrategia se crea un modelo probabilístico en función de los hiperparámetros. Para ello, se eligen los parámetros en función de su probabilidad de mejorar las métricas. Su funcionamiento es correcto para un número no muy alto de parámetros.

Una vez hemos seleccionado la estrategia tenemos que decidir cuáles serán los parámetros que podemos usar. Dependiendo de nuestro código, podremos elegir el tipo de parámetro, teniendo que hacer pequeñas adaptaciones al mismo para poder hacer uso de ellos.

\subsubsection{Barrido de parámetros con selección Bayesiana}\label{sec:sweep_experiments_01}
En este primer experimento, hemos configurado un barrido de parámetros usando una estrategia Bayesiana en la que los objetivos eran maximizar \textit{f\_beta} y minimizar la función de pérdida. Para ello hemos dispuesto una configuración de \textit{epochs} de entre 5 y 10, y una tasa de aprendizaje de entre 0,1 y 0,0001. Para los optimizadores hemos elegido \textit{ADAM} y \textit{SGD} con sus valores por defecto.

\figura{1}{img/sweep-01.png}{Experimento de barrido de parámetros con estrategia Bayesiana. Los objetivos son maximizar la métrica f\_beta y minimizar la función de pérdida.}{fig:bayes_sweep}{}

Los resultados mostraron que el optimizador \textit{SGD} se comporta mejor, en cuanto a la métrica f\_beta para la mayoría de ejecuciones. En concreto, la ejecución A, con optimizador SGD, consiguió los mejores resultados en cuanto a la métrica de la función de pérdida del conjunto de validación \textit{val\_loss}. Véanse el cuadro \ref{tab:sweep_simple_vgg16} y la imagen \ref{fig:bayes_sweep}

\cuadro{|l|lllllllcc|}{Barrido de parámetros con el \textit{encoder} VGG-16, y el dataset simple}{tab:sweep_simple_vgg16}
{
    \hline
    Name  & f\_beta  & f1\_mean & pr\_mean & re\_mean & precision & recall & \textit{loss} & \textit{epochs} & \textit{best} \\
    \hline
    A-SGD  & \textbf{0.7459}  & 0.0752   & 0.5563   & 0.0849   & 0.7542    & 0.7378 & \textbf{0.6184} & 10 & 8  \\
    B-SGD  & 0.7320  &   -      &    -     &    -     & 0.7402    & 0.7241 & 0.6436 & 9  & 1  \\
    C-SGD  & 0.7204  & 0.3574   & 0.6210   & 0.2753   & 0.7285    & 0.7125 & 0.6526 & 10 & 10 \\
    D-SGD  & 0.7201  & 0.0787   & 0.5398   & 0.0813   & 0.7282    & 0.7123 & 0.6729 & 10 & 6  \\
    E-SGD  & 0.7024  & 0.0840   & 0.4941   & 0.0894   & 0.7101    & 0.6948 & 0.6808 & 9  & 8  \\
    F-SGD  & 0.6998  &   -      &    -     &    -     & 0.7075    & 0.6922 & 0.8957 & 7  & 7  \\
    G-SGD  & 0.6713  & 0.0860   & 0.5252   & 0.0890   & 0.6786    & 0.6641 & 0.7424 & 10 & 7  \\
    H-Adam & 0.6278  & 0.0727   & 0.5121   & 0.0810   & 0.6349    & 0.6209 & 3.1234 & 5  & 4  \\
    \hline
}

\subsubsection{Barrido de parámetros multiagente}\label{sec:sweep_experiments_02}
El siguiente experimento consiste en un refinamiento del anterior. Para ello hemos usado más agentes en distintas máquinas. Por un lado hemos usado el servidor Deepknowledge y por otro, hemos usado Google Colaboratory\footnote{https://colab.research.google.com} para poder hacer muchas ejecuciones explorando el árbol de parámetros.

Finalmente hicimos 32 ejecuciones del proyecto eligiendo una estrategia aleatoria. Los hiperparámetros que propusimos fueron un conjunto discreto de \textit{epochs} de 4, 6 y 8 iteraciones. La tasa de aprendizaje estaría comprendida entre 0.1 y 0.0001 eligiendo los valores según una distribución uniforme. Cuando optásemos por el optimizador SGD, el \textit{momentum} podría variar entre los valores 0.88 y 0.9 dado que en el anterior experimento se observó que en un intervalo cercano a 0.9 se conseguían los mejores resultados.

Para conformar la tabla de resultados, vamos a filtrar aquellas ejecuciones que no finalizaron o que fueron interrumpidas y aquellas que no presentan un resultado para \textit{val\_loss}

\cuadro{|l|lllccc|}{Barrido de parámetros con el \textit{encoder} ResNet-50, y el dataset simple}{tab:sweep_simple_resnet50}
{
    \hline
    Name        & momentum & f\_beta & f1\_mean & epochs & best\_epoch & best\_val\_loss \\
    \hline
    A-sgd       & 0.90     & \textbf{0.5899}  & 0.5246 & 8      & 6           & \textbf{1.5682} \\
    B-adam      & 0.90     & 0.5892  & 0.0077   & 4      & 1           & 11.2174         \\
    C-adam      & 0.88     & 0.5848  & 0.0015   & 8      & 2           & 6.3988          \\
    D-adam      & 0.90     & 0.5769  & 0.0080   & 8      & 1           & 15.6653         \\
    E-sgd       & 0.88     & 0.5725  & 0.4946   & 6      & 6           & 6.9204          \\
    F-adam      & 0.88     & 0.5573  & 0.7140   & 8      & 1           & 5.8076          \\
    G-sgd       & 0.90     & 0.5498  & 0.0894   & 8      & 3           & 2.1497          \\
    H-adam      & 0.90     & 0.5494  & 0.0127   & 8      & 7           & 5.8076          \\
    I-sgd       & 0.90     & 0.5458  & 0.5571   & 6      & 6           & 3.7967          \\
    J-adam      & 0.90     & 0.5458  & 0.4600   & 8      & 4           & 5.8076          \\
    K-sgd       & 0.88     & 0.5441  & 0.1186   & 4      & 2           & 5.3944          \\
    L-adam      & 0.90     & 0.5432  & 0.0127   & 6      & 1           & 11.2174         \\
    M-sgd       & 0.90     & 0.5431  & 0.0670   & 8      & 5           & 2.8243          \\
    N-adam      & 0.88     & 0.5430  & 0.0127   & 4      & 1           & 11.1260         \\
    O-adam      & 0.88     & 0.5408  & 0.0195   & 4      & 1           & 11.1260         \\
    P-adam      & 0.90     & 0.5387  & 0.0077   & 4      & 1           & 15.6653         \\
    Q-adam      & 0.90     & 0.5285  & 0.0615   & 4      & 1           & 5.7321          \\
    R-sgd       & 0.88     & 0.5262  & 0.4510   & 4      & 4           & 5.5984          \\
    S-sgd       & 0.88     & 0.5158  & 0.1220   & 6      & 2           & 5.5485          \\
    T-sgd       & 0.90     & 0.4972  & 0.0445   & 4      & 1           & 10.9544         \\
    U-sgd       & 0.88     & 0.4886  & 0.7140   & 4      & 4           & 2.8945          \\
    V-adam      & 0.90     & 0.4787  & 0.0077   & 6      & 1           & 15.6485         \\
    W-sgd       & 0.88     & 0.4739  & 0.5748   & 6      & 4           & 3.9738          \\
    X-adam      & 0.90     & 0.4349  & 0.0000   & 4      & 1           & 5.8076          \\
    Y-adam      & 0.90     & 0.4319  & 0.0077   & 4      & 1           & 11.2174         \\
    \hline
}

\figura{1}{img/sweep-02-best-beta.png}{Experimento de barrido de parámetros con estrategia Aleatoria. Hemos filtrado aquellos parámetros que estaban incompletos. Hemos destacado la ejecución con mejor valor para  \textit{f\_beta}.}{fig:sweep-02-best-beta}{}

Según se muestra en el gráfico \ref{fig:sweep-02-best-beta} el barrido de parámetros es bastante completo, estando bien distribuidos los entrenamientos según el número de iteraciones, tasa de aprendizaje, \textit{momentum} y optimizador. Para hacer una mejor lectura de los mismos haremos uso de la herramienta que WandB nos proporciona llamada \textit{importance} o importancia.

Para calcular esta métrica, se entrena un \textit{random forest} con los los valores de los hiperparámetros como entrada y las métricas como objetivos. Con esta métrica sintética, conseguimos desentrañar las interacciones entre hiperparámetros de modo que pueden servir para decidir cuáles son mejores respecto a otros o cuáles interesa cambiar.

Gracias a esta herramienta podemos ver la correlación, directa o inversa del hiper-parámetro con respecto a una métrica. En nuestro caso presentaremos los resultados para \textit{val\_loss} y \textit{f\_beta}. Una alta correlación significa que para un valor alto del hiper-parámetro tendremos un valor alto de la métrica y viceversa. La correlación inversa nos proporciona la misma información pero de distinto sentido. Un valor alto del hiper-parámetro corresponderá a un valor bajo de la métrica, que en el caso de querer minimizar el valor de la última, es de gran ayuda.

\figura{1}{img/importance-sweep-02.png}{Importancia de los hiperparámetros con respecto a las métricas \textit{val\_loss} y \textit{f\_beta}}{fig:importance_sweep_02}{}

De este modo, podemos ver que el \textit{learning rate} es de gran importancia para conseguir un buen valor del entrenamiento con respecto a \textit{val\_loss} y \textit{f\_beta}. También apreciamos que el \textit{momentum} está correlacionado con la métrica \textit{val\_loss} aunque no es importante y el número de \textit{epochs} está relacionado directamente con \textit{val\_loss} y \textit{f\_beta}.

\section{Conclusiones}
Pese a la gran herramienta que supone el barrido de hiperparámetros, el conjunto de datos de entrenamiento no era lo suficientemente grande como para conseguir buenos resultados en la segmentación. Por tanto podemos concluir que los aspectos más relevantes para entrenar al conjunto de datos son, por un lado disponer de un conjunto de datos lo suficientemente extenso para hacer un entrenamiento de calidad y por otro, ejecutar el entrenamiento durante el suficiente número de iteraciones como para conseguir resultados relevantes.