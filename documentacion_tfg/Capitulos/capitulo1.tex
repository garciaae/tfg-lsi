% !TEX root = ../proyect.tex

\chapter{Introducci\'on}\label{cap1}
Las imágenes aéreas tienen gran utilidad en multitud de aplicaciones de distintos ámbitos. Pese a que el análisis de estos datos mediante técnicas basadas en redes neuronales va algo retrasado en comparación con otros conjuntos de datos que están formados por escenas tomadas en ciudades como \textit{CityScapes} \cite{cordts2016cityscapes} u otros compuestos por objetos cotidianos como PASCAL VOC \cite{mottaghi_cvpr14}, en este proyecto ahondaremos en ese problema y presentaremos alternativas que haciendo uso de distintas técnicas existentes y usando variaciones en los hiper-parámetros permitan mejorar esos valores. Asimismo haremos un recorrido por el estado del arte en segmentación de imágenes.

\section{Introdución}\label{sec:introduccion}
En este trabajo se propone hacer un recorrido descriptivo del estado del arte en la segmentación de imágenes, especialmente en imágenes aéreas obtenidas mediante \textit{drone} o satélite y una primera aproximación a cómo implementarlo.

El documento se ha dividido en varias secciones. En primer lugar se hace una descripción de conceptos, visión artificial, \textit{machine learning}, tratamiento de imágenes etc.

En una segunda parte se describen tanto las herramientas utilizadas como el código empleado para llevar a cabo los experimentos.

En tercer lugar hacemos un recorrido por los distintos proyectos, experimentos y algoritmos empleados en la literatura.

Por último, presentaremos una serie de validaciones de los algoritmos empleados sobre un \textit{dataset} relevante para nuestros propósitos. Compararemos sus resultados y evolución en función de los distintos parámetros de entrenamiento: extensión del \textit{dataset}, número de iteraciones y técnicas de detección empleadas.

\section{Motivaci\'on}\label{sec:motivacion}

Las razones para elegir este proyecto como trabajo de fin de grado son claras: se trata de una tecnología revolucionaria que tiene el potencial de cambiar la forma de proceder en varias áreas y proporcionar un conjunto de soluciones que pueden ser de gran relevancia en aspectos tan diversos como reconocimiento de enfermedades en imágenes médicas, detección de catástrofes o conducción autónoma.

\section{Objetivos generales}\label{sec:objetivos_generales}
\textit{}
Los objetivos generales de este proyecto son por un lado describir las técnicas empleadas para resolver un problema real, la segmentación y por otro, adquirir la formación y destreza en las tareas de segmentación y clasificación de objetos usando técnicas de \textit{Deep Learning} sobre conjuntos de imágenes aéreas.

El proyecto se puede considerar satisfactorio si conseguimos alcanzar esos conocimientos. No se ha de ligar el funcionamiento de uno u otro proyecto en particular al buen término de éste proyecto porque no es ese el fin último. Su objetivo es más ahondar sobre el estado del arte en la materia que ofrecer una implementación de un problema concreto.

\section{Trabajos existentes}\label{sec:segmentation_information}
Actualmente, en el ámbito de la visión por ordenador, las métricas usadas para evaluar este tipo de trabajos están alcanzando valores muy altos, de hasta el 85\% \textit{Intersection Over Union (mIOU)} en CityScapes \cite{tao2020hierarchical} un \textit{dataset} compuesto por escenas urbanas, 83,5\% en en \textit{Camvid} \cite{zhu2019improving} formado por escenas de vídeo de alta calidad tomadas desde la perspectiva de un coche y del 90\% en PASCAL 2012 \cite{zoph2020rethinking} un conjunto de imágenes de objetos cotidianos divididos en 20 clases. Estos son valores realmente buenos para la segmentación de imágenes en general pero son algo más difíciles de alcanzar para imágenes aéreas.

La resolución espacial o \textit{Instantaneous Field of View (IFOV)} es la medida del menor objeto que puede ser resuelto por un sensor o bien la porción de terreno capturada por el campo de visión del sensor \cite{wang2020}. Se pueden considerar de bajas resoluciones a aquellas imágenes mayores a 1000 m/pixel, de resolución media las que están entre 100 y 1000 metros, alta resolución a las que están entre 5 y 100 metros y por último, de muy alta resolución a aquellas cuya resolución está por debajo de los 5 metros por pixel \cite{wang2020}. 

\figura{0.90}{img/different_resolutions.jpg}{Imagen aérea captada a cuatro resoluciones diferentes. Fuente: Wang 2020}{fig:resolutions}{}

La resolución espacial juega un importante papel en aplicaciones como la determinación de zonas urbanas densas, o para la detección de objetos pequeños. No debemos interpretar una baja resolución como un problema: los sensores de baja resolución son útiles para detectar problemas de gran escala, como vertidos de petróleo, o extensiones en los incendios. Indudablemente, los nuevos \textit{datasets} de muy alta resolución permiten el desarrollo de nuevas aplicaciones y detectar hitos difícilmente detectables anteriormente, como pequeñas zonas verdes en el interior de grandes aglomeraciones urbanas o aforamiento de eventos al aire libre.

El número de dispositivos y sensores disponibles ha aumentado y por tanto los \textit{datasets} disponibles también. Asimismo, la sensibilidad y resolución de los mismos han proporcionado que los conjuntos de imágenes disponibles sean cada vez de mayor calidad con resoluciones por satélite de hasta 50 cm por píxel, como el \textit{dataset} Pléiades, disponible comercialmente \cite{Damilano2001PleiadesHR} y en el que cada píxel representa un área cuadrada de 0.5 metros de lado. Los satélites comúnmente usados como el Landsat 8 o los que forman parte de la red Sentinel-2 tienen una resolución de 16 bits frente a los 8 bit de generaciones anteriores lo que incide en mayores resoluciones.


\section{Inteligencia y visi\'on artificial}\label{sec:inteligencia_y_vision}
La \textit{inteligencia artificial} es un conjunto de técnicas en las cuales existen diferentes vertientes y clasificaciones. El paradigma más reciente hace ver a \'esta, como un sistema de predicción e interpretación de conjuntos de datos o \textit{datasets},  cada vez más frecuentes y p\'ublicamente accesibles.

Entre los problemas que resuelve la Inteligencia Artificial, IA, se encuentra el reconocimiento de formas, patrones y objetos más o menos complejos en imágenes. Este reconocimiento consiste en hacer interpretar a una máquina qué es lo que ve, tratando de imitar el modo en que lo hace una persona.

Para ello tendremos que hacer que la máquina aprenda, usando una serie de técnicas que reciben el nombre genérico de \textit{Machine Learning}, ML, o aprendizaje automático.

Podr\'iamos dividir la forma en la que los sistemas resuelven los problemas dependiendo de si necesitan conocer los resultados de salida, aprendizaje supervisado, o si el \'este se produce sin que el sistema conozca a priori qu\'e va a aprender, aprendizaje no supervisado.

Este conjunto de t\'ecnicas representa un cambio de paradigma con respecto a la forma cl\'asica de resoluci\'on de problemas, en el que en lugar de presentar un conjunto de datos de entrada y unas reglas para obtener unas respuestas, presentaremos unos casos concretos junto con etiquetas que los representan y esperamos obtener las reglas que los relacionan.

\subsection{Visi\'on artificial}
La aproximación en la que está basada la visión artificial son las llamadas redes neuronales convolucionales y estas en el cortex visual.

En un experimento en el ámbito de la neurociencia \cite{Hubel1962}, se demostró que ciertas neuronas en el cerebro de animales solo respondían a la presencia de vértices o líneas en una determinada orientación. Así algunas neuronas reaccionaban a diagonales otras a horizontales, vértices o movimiento.
Esta idea de componentes especializados es lo que subyace detrás de las redes neuronales convolucionales. As\'i, modelaremos estructuras en el sistema que mimeticen tanto la estructura de una neurona como su funcionamiento individual y de conjunto.
