% !TEX root = ../proyect.tex

\chapter{Conceptos generales}

\section{Conceptos}\label{sec:conceptos}
Existen diferentes conceptos que se nombran en este trabajo, muchos de ellos en el campo de la inteligencia artificial y del procesamiento de imágenes.

\subsection{Machine learning}\label{sec:machine_learning}
\textit{Machine learning} es un conjunto de técnicas en el que un sistema, mediante unas procedimientos determinados, aprende a resolver un problema con poca o nula intervención humana. Si tanto los datos de entrada como la salida esperada se conocen de antemano, hablamos de aprendizaje supervisado. En el caso de no tener intervención humana, es decir, datos de entrada sin etiquetar para buscar una agrupación de los mismos o un conjunto de reglas que los asocien, hablamos de aprendizaje no supervisado.

\subsection{Redes neuronales}\label{redes_neuronales}
Las redes neuronales constituyen un conjunto de algoritmos enfocados en el reconocimiento de patrones en los datos de entrada que forman una familia de técnicas dentro del \textit{Machine learning}.

La unidad básica de la red neuronal es la neurona o perceptrón y consiste en una estructura lógica que pretende mimetizar su homónima biológica. De tal forma, tendremos:
\begin{itemize}
    \item Algo similar a las dendritas, representado por las diferentes entradas a nuestro algoritmo.
    \item Pesos que permitan elegir qué entradas son las más relevantes.
    \item Una función de transferencia a modo de núcleo que haría de \textit{sumador}.
    \item La función de activación que determina si se tiene en cuenta la entrada en función del umbral, valor este último independiente de los de entrada.\\
    Entre las funciones de activación más comunes están la sigmoide, con forma de S y un rango entre 0 y 1, la tangente hiperbólica Tanh cuyo rango, centrado, está ente -1 y 1 y por último ReLu o \textit{Rectified Linear Unit} cuyo cálculo es el más simple y el más usado en los modelos de \textit{Deep learning} (Bhardwaj et al., 2018)
    \item Una salida que funciona a semejanza de como lo hace el axón en la neurona biológica
\end{itemize}

\subsection{Componentes de una red neuronal convolucional}\label{sec:componentes_cnn}
Las redes neuronales convolucionales, CNN, son algoritmos que tienen como entrada imágenes y como salida la descripción de las características que la componen bien etiquetando lo que hay en ellas, bien clasificando cada uno de los píxeles que la conforman.
La primera capa que compone una CNN se denomina capa convolucional. Está formada por aplicaciones sucesivas del kernel a lo largo de toda la superficie de la capa de entrada o \textit{input layer} y su función es extraer características de la capa de entrada.

El \textit{kernel}, neurona o filtro es una región cuadrada de lado típicamente 3 o 5 que se aplica sucesivamente sobre los datos de entrada para extraer algún tipo de característica oculta de los mismos. Al movimiento de desplazamiento o convolución se le aplican ciertos parámetros como el salto o \textit{stride}.
A la proyección del kernel sobre la imagen le llamaremos campo receptivo, que intuitivamente recibe el mismo nombre que empleamos para designar las células receptivas de los distintos órganos sensoriales del cuerpo.

A las aplicaciones sucesivas de distintas capas se les llama arquitectura. En una CNN, partimos de una entrada, aplicamos convolución y acabamos teniendo una capa densa o totalmente conectada. Llegaremos a ella aplicando sucesivamente convolución y otro tipo de transformaciones que irán cambiando las dimensiones de la capa de entrada.
En cada iteración, la entrada de una capa es, al menos, la salida de la anterior y van aportando información de las características que se van buscando.

Al ir profundizando en la red, los mapas de activación que se obtienen son cada vez más sensibles a características más complejas y en zonas más amplias del conjunto de datos de entrada.

Al final de una arquitectura de clasificación encontramos normalmente un vector de N dimensiones, representando cada uno de ellos la probabilidad de que la característica se encuentre en la entrada.

En un problema de segmentación tendremos que remontar usando como base esta capa para llegar a una salida del mismo tamaño que la entrada, en la que cada valor esté etiquetado con una etiqueta del género de interés. Por ejemplo, si aportamos una imagen en la que hay un conjunto de personas y queremos segmentar la imagen, hemos de decir qué píxel es persona y cual no lo es.

\subsection{Deep learning}\label{sec:deep_learning}

El \textit{Deep learning} o aprendizaje profundo es un conjunto de técnicas en las que se intenta, partiendo de un conjunto de datos de alto nivel expresados de forma matricial, como pueden ser imágenes, sonidos u otro tipo de observaciones, extraer información de interés a partir de ellos. Habitualmente, los conjuntos de datos tienen muchas entradas las cuales pueden ser recopiladas del medio natural o bien ser sintéticas, generadas por distintas técnicas como pueden ser \textit{renderizado} o aumento de datos (\textit{data augmentation}).

Dicho conjunto de técnicas no tienen una definición formal más allá de compartir un conjunto de características comunes:
\begin{itemize}
    \item Usar una estructura de capas de neuronas interconectadas en la que la entrada de una es la salida de la anterior.
    \item Cada capa hace una transformación de los datos.
    \item El conjunto de capas forman una jerarquía de abstracción, partiendo de más bajo a más alto.
    \item El número de capas ha de ser, al menos, de dos para considerarlo como \textit{profundo}.
\end{itemize}

\section{Alternativas y estado del arte}\label{sec:alternativas}

La idea inicial de este proyecto era la de, a partir de unas imágenes aéreas tomadas por un avión, estimar el área de las zonas que habían resultado afectadas por un incendio y segmentarlas en un rango de afectación: totalmente devastadas, muy afectadas, parcialmente afectadas y no afectadas.

\subsection{Datasets}\label{sec:descripcion_detallada}
Dado que el conjunto de datos planeado no se encontraba disponible a la hora de comenzar a trabajar en él, se optó por explorar otras vías.

En un principio se hizo una primera selección de \textit{datasets} e implementaciones existentes que abordasen esa segmentación y así tener un punto de partida para iniciar el trabajo. Para ello se recurrió a la colección de datasets de imágenes etiquetadas en Awesome Satellite \cite{awesomesatellite}. Este \textit{website}\footnote{https://awesomeopensource.com/project/chrieke/awesome-satellite-imagery-datasets} contiene una lista de \textit{datasets} de imágenes aéreas y de satélite con etiquetadas y que sirven como datos de entrada para entrenar un sistema. Está organizado de modo que los conjuntos de datos más recientes se encuentran en la parte superior de cada categoría. Las categorías que encontraremos son: segmentación de instancias, detección de objetos, segmentación semántica, clasificación de escenas y otros sin clasificar.

De entre ese conjunto de datos se tomaron dos como punto de partida. Por un lado el dataset de DroneDeploy \cite{dronedeploy} que contiene tanto la descripción del conjunto de datos como las instrucciones para usarlo.

Por otro lado se investigó el \textit{dataset} de xView2, xBD \cite{gupta2019xbd} Annotated high-resolution satellite imagery for building damage assessment, por ofrecer datos de antes y después de desastres naturales, polígonos de edificios etiquetados con el nivel de daños de 0 a 3, siendo 0 un edificio indemne y 3 uno totalmente destruido. Este \textit{dataset} También ofrece otros \textit{metadatos} procedentes del satélite junto con recuadros delimitadores y etiquetas para factores ambientales como el fuego, el agua y el humo. El uso de imágenes aéreas debidamente procesadas con algoritmos de procesado de imágenes permiten evaluar los daños y ayudan a disminuir los riesgos para las personas. xBD es el mayor conjunto de datos de evaluación de daños en edificios hasta la fecha, y contiene 850.736 anotaciones de edificios en 45.362 km de imágenes.
