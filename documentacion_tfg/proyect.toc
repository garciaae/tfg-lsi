\babel@toc {spanish}{}
\contentsline {chapter}{\'Indice general}{III}
\contentsline {chapter}{{\'I}ndice de cuadros}{V}
\contentsline {chapter}{\'{I}ndice de figuras}{VI}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}
\contentsline {section}{\numberline {1.1}Introduci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.2}Motivaci\'on}{1}
\contentsline {section}{\numberline {1.3}Objetivos generales}{2}
\contentsline {section}{\numberline {1.4}Trabajos existentes}{2}
\contentsline {section}{\numberline {1.5}Inteligencia y visi\'on artificial}{3}
\contentsline {subsection}{\numberline {1.5.1}Visi\'on artificial}{3}
\contentsline {chapter}{\numberline {2}Conceptos generales}{4}
\contentsline {section}{\numberline {2.1}Conceptos}{4}
\contentsline {subsection}{\numberline {2.1.1}Deep learning}{4}
\contentsline {subsection}{\numberline {2.1.2}Redes neuronales}{4}
\contentsline {subsection}{\numberline {2.1.3}Machine learning}{5}
\contentsline {subsection}{\numberline {2.1.4}Componentes de una red neuronal convolucional}{5}
\contentsline {section}{\numberline {2.2}Alternativas y estado del arte}{5}
\contentsline {subsection}{\numberline {2.2.1}Datasets}{5}
\contentsline {chapter}{\numberline {3}Segmentaci\'on}{7}
\contentsline {section}{\numberline {3.1}Qu\'e es la segmentaci\'on}{7}
\contentsline {section}{\numberline {3.2}Tipos de segmentaci\IeC {\'o}n}{7}
\contentsline {section}{\numberline {3.3}Los desaf\IeC {\'\i }os de la segmentaci\IeC {\'o}n de im\IeC {\'a}genes a\IeC {\'e}reas}{8}
\contentsline {section}{\numberline {3.4}Arquitecturas de los algoritmos de segmentaci\IeC {\'o}n}{9}
\contentsline {subsection}{\numberline {3.4.1}Redes neuronales profundas}{9}
\contentsline {subsubsection}{Redes neuronales convolucionales}{9}
\contentsline {subsubsection}{Redes neuronales recurrentes}{9}
\contentsline {subsubsection}{Arquitecturas Encoder-Decoder y Auto-Encoders}{10}
\contentsline {subsubsection}{Redes generativas antag\IeC {\'o}nicas}{10}
\contentsline {subsection}{\numberline {3.4.2}Modelos de segmentaci\IeC {\'o}n basados en Deep learning}{10}
\contentsline {subsubsection}{Redes completamente convolucionales}{11}
\contentsline {subsubsection}{Redes convolucionales con modelos gr\IeC {\'a}ficos}{11}
\contentsline {subsubsection}{Modelos DL basados en codificador-decodificador}{11}
\contentsline {subsubsection}{Segmentaci\IeC {\'o}n de im\IeC {\'a}genes de prop\IeC {\'o}sito general}{11}
\contentsline {subsubsection}{Segmentaci\IeC {\'o}n de im\IeC {\'a}genes m\IeC {\'e}dicas y biol\IeC {\'o}gicas}{12}
\contentsline {subsubsection}{Redes en pir\IeC {\'a}mide y multiescala}{13}
\contentsline {subsubsection}{Arquitecturas de redes neuronales convolucionales aplicadas en una zona}{14}
\contentsline {subsubsection}{Modelos de convoluci\IeC {\'o}n hueca o dilatada}{15}
\contentsline {subsubsection}{Modelos basados en redes neuronales recurrentes}{16}
\contentsline {subsection}{\numberline {3.4.3}Arquitecturas basadas en mecanismos de atenci\IeC {\'o}n}{16}
\contentsline {subsection}{\numberline {3.4.4}Modelos generativos y entrenamiento antag\IeC {\'o}nico}{17}
\contentsline {subsection}{\numberline {3.4.5}Modelos convolucionales con contornos activos}{17}
\contentsline {section}{\numberline {3.5}Manos en el c\'odigo}{18}
\contentsline {subsection}{\numberline {3.5.1}Entrenar el modelo}{18}
\contentsline {subsection}{\numberline {3.5.2}Operaciones}{18}
\contentsline {subsubsection}{Down-sampling}{18}
\contentsline {subsubsection}{Up-sampling}{19}
\contentsline {subsection}{\numberline {3.5.3}Uniendo las piezas: elegir una arquitectura}{19}
\contentsline {subsubsection}{Arquitectura U-Net}{19}
\contentsline {chapter}{\numberline {4}Dise\IeC {\~n}o de la soluci\IeC {\'o}n}{21}
\contentsline {section}{\numberline {4.1}Arquitectura de la soluci\IeC {\'o}n}{21}
\contentsline {subsection}{\numberline {4.1.1}Elecci\IeC {\'o}n del conjunto de datos}{21}
\contentsline {subsection}{\numberline {4.1.2}An\IeC {\'a}lisis de las herramientas}{22}
\contentsline {subsection}{\numberline {4.1.3}Elecci\IeC {\'o}n de la plataforma para la presentaci\IeC {\'o}n de resultados}{22}
\contentsline {section}{\numberline {4.2}Descripci\IeC {\'o}n del sistema y resultados}{24}
\contentsline {subsection}{\numberline {4.2.1}Bifurcando el c\IeC {\'o}digo}{24}
\contentsline {subsection}{\numberline {4.2.2}Haciendo funcionar el c\IeC {\'o}digo}{24}
\contentsline {subsection}{\numberline {4.2.3}An\IeC {\'a}lisis del rendimiento en el modelo presentado}{24}
\contentsline {subsubsection}{Experimento base. Ejecuci\IeC {\'o}n sin modificaciones}{25}
\contentsline {subsubsection}{Variaciones en los \textit {encoders}}{25}
\contentsline {subsubsection}{Variaciones en los \textit {epochs}}{26}
\contentsline {subsection}{\numberline {4.2.4}Barrido de hiperpar\IeC {\'a}metros}{28}
\contentsline {subsubsection}{Experimentos con barridos de par\IeC {\'a}metros}{28}
\contentsline {subsubsection}{Barrido de par\IeC {\'a}metros con selecci\IeC {\'o}n Bayesiana}{29}
\contentsline {subsubsection}{Barrido de par\IeC {\'a}metros multiagente}{29}
\contentsline {chapter}{\numberline {5}Conclusiones finales}{33}
\contentsline {section}{\numberline {5.1}Conclusiones}{33}
\contentsline {section}{\numberline {5.2}Trabajo futuro}{33}
\contentsline {chapter}{\numberline {6}Planificaci\IeC {\'o}n y costes}{34}
\contentsline {section}{\numberline {6.1}Metodolog\IeC {\'\i }a}{34}
\contentsline {section}{\numberline {6.2}Planificaci\IeC {\'o}n}{34}
\contentsline {chapter}{Referencias}{35}
\contentsfinish 
