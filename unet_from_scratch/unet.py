import logging
import os
import random

import numpy as np
from skimage.io import imread, imsave
from skimage.transform import resize
from skimage.color import gray2rgb
import tensorflow as tf
from tqdm import tqdm

# Fix random seed to repeat the experiment
np.random.seed = 42

IMG_WIDTH = 128
IMG_HEIGHT = 128
IMG_CHANNELS = 3

TRAIN_PATH = 'Unet/stage1_train/'
TEST_PATH = 'Unet/stage1_test/'
PROCESSED_PATH = 'Unet/stage1_train_processed/'

train_ids = next(os.walk(TRAIN_PATH))[1]
test_ids = next(os.walk(TEST_PATH))[1]


X_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
Y_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool8)


logger = logging.getLogger(__name__)


def resize_images(images_iterator):
    # Cambiar las dimensiones de las imagenes de prueba a 128 x 128
    for image_idx, image_name in tqdm(enumerate(images_iterator), total=len(images_iterator)):
        path = TRAIN_PATH + image_name
        img = imread(path + '/images/' + image_name + '.png')[:, :, :IMG_CHANNELS]
        img = resize(img, (IMG_HEIGHT, IMG_WIDTH, 1), mode='constant', preserve_range=True)
        X_train[image_idx] = img
        imsave(f'{PROCESSED_PATH}images/{image_name}.png', img.astype(np.uint8))

        mask = np.zeros((IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool8)
        # Mezclar todas las mascaras en una, porque vienen en archivos separados.
        for mask_file in next(os.walk(path + '/masks/'))[2]:
            mask_ = imread(path + '/masks/' + mask_file)
            mask_ = np.expand_dims(
                resize(
                    mask_,
                    (IMG_HEIGHT, IMG_WIDTH),
                    mode='constant',
                    preserve_range=True
                ),
                axis=-1,
            )
            # Numpy maximum superpone las matrices y devuelve el maximo de cada elemento
            # np.maximum(np.maximum([0, 1, 0], [0, 3, 0]), [0, 2, 1])
            # Out: array([0, 3, 1])
            mask = np.maximum(mask, mask_)
        Y_train[image_idx] = mask
        imsave(f'{PROCESSED_PATH}masks/{image_name}.png', mask.astype(np.uint8))

for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
    image_path = f'{PROCESSED_PATH}images/{id_}.png'
    mask_path = f'{PROCESSED_PATH}masks/{id_}.png'
    X_train[n] = gray2rgb(imread(image_path))
    Y_train[n] = np.expand_dims(imread(mask_path), axis=-1)

X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
sizes_test = []

# Cambiar el tamano de las imagenes de prueba tambien a 128 x 128
# for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
#     path = TEST_PATH + id_
#     img = imread(path + '/images/' + id_ + '.png')[:, :, :IMG_CHANNELS]
#     sizes_test.append([img.shape[0], img.shape[1]])
#     img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
#     X_test[n] = img
#     imsave(f'{PROCESSED_PATH}test/{id_}.png', img.astype(np.uint8))

for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
    test_path = f'{PROCESSED_PATH}test/{id_}.png'
    X_test[n] = gray2rgb(imread(test_path))


# Construir el modelo con la arquitectura U-Net
logger.info("Building the model")
inputs = tf.keras.layers.Input((IMG_WIDTH, IMG_HEIGHT, IMG_CHANNELS))

# Cambiar los valores del rango 0-255 de una imagen RGB a 0-1
logger.info("Changing input size to be in 0..1 range")
s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

# Rama contractiva o encoder
# Kernel initializer pesos iniciales para el kernel: distribucion normal, ortogonal, truncada, etc.
# pixeles al hacer la convolucion
# Activation ReLU 0 si x < 0, f(x) si x>=0 o dicho de otro modo, max(0, f(x))
# https://es.wikipedia.org/wiki/Rectificador_(redes_neuronales)
# he_normal actualizar los pesos iniciales con una distribucion normal truncada
# Padding 'same' para que la imagen de salida tenga el mismo tamaño que la de entrada anadiendo

c1 = tf.keras.layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(s)
c1 = tf.keras.layers.Dropout(rate=10/100)(c1)  # Dropout de un 10% para no sobreentrenar
c1 = tf.keras.layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
p1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c1)

c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p1)
c2 = tf.keras.layers.Dropout(0.1)(c2)
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p2)
c3 = tf.keras.layers.Dropout(0.1)(c3)
c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p3)
c4 = tf.keras.layers.Dropout(0.2)(c4)
c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
p4 = tf.keras.layers.MaxPooling2D((2, 2))(c4)

c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same',)(p4)
c5 = tf.keras.layers.Dropout(0.3)(c5)
c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

# Rama expansiva o decoder
u6 = tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=(2, 2), strides=(2, 2), padding='same',)(c5)
u6 = tf.keras.layers.concatenate(inputs=[u6, c4])
c6 = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(rate=20/100)(c6)
c6 = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

u7 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same',)(c6)
u7 = tf.keras.layers.concatenate([u7, c3])
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
c7 = tf.keras.layers.Dropout(0.2)(c7)
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same',)(c7)
u8 = tf.keras.layers.concatenate([u8, c2])
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u8)
c8 = tf.keras.layers.Dropout(0.1)(c8)
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c8)

u9 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same',)(c8)
u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u9)
c9 = tf.keras.layers.Dropout(0.1)(c9)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c9)

outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
# Sthochastic gradient descend, root mean square propagation, degrade at a delta
# Binary-crossentropy clasificacion binaria, es del tipo de interes o no es del tipo de interes
logger.info("Compiling the model")
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model.summary()

# Parametros entrenables Connections + biases + outputs

# Model fitting
# Validation split: porcentaje de datos para validar
# Callbacks
# Poner pocos epochs underfitting. Poner muchos, overfitting.
# Earlystopping Parar si el no hay progreso en los patience=3 ultimos epochs
# Tensorboard Graphical Interface para ver el validation_loss de cada epoch. Un dashboard.

# Model Checkpoint
# Checkpoints. Puntos en los que se guarda el progreso para no perderlo si algo pasa
checkpointer = tf.keras.callbacks.ModelCheckpoint('model_for_something.h5', verbose=1, save_best_only=True)

callbacks = [
    tf.keras.callbacks.EarlyStopping(patience=2, monitor='val_loss'),
    tf.keras.callbacks.TensorBoard(log_dir='logs'),
]

# Conservar un 10% de los datos para la validacion: validation_split=0.1
logger.info("Training the model...")
results = model.fit(X_train, Y_train, validation_split=0.1, batch_size=16, epochs=25, callbacks=callbacks)

# Guardar el modelo
logger.info("Saving the model")
model.save('model_unet_{}.h5'.format(tf.keras.__version__.replace('.', '_')))

# Comprobar los resultados
# Elegimos una imagen de testing, otra de validation
logger.info("Checking the results. Predicting a value")

idx = random.randint(0, len(X_train))
preds_train = model.predict(X_train[:int(X_train.shape[0]*0.9)], verbose=1)
preds_val = model.predict(X_train[int(X_train.shape[0]*0.9):], verbose=1)
preds_test = model.predict(X_test, verbose=1)

# Convertir los valores por encima de 0.5 en 1
preds_train_t = (preds_train > 0.5).astype(np.uint8)
preds_val_t = (preds_val > 0.5).astype(np.uint8)
preds_test_t = (preds_test > 0.5).astype(np.uint8)
