import os
import random

import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread
from skimage.color import gray2rgb
from tensorflow.keras.models import load_model
from tensorflow.keras import __version__ as version
from tqdm import tqdm

# Fix random seed to repeat the experiment
np.random.seed = 42

IMG_WIDTH = 128
IMG_HEIGHT = 128
IMG_CHANNELS = 3

TRAIN_PATH = 'Unet/stage1_train/'
TEST_PATH = 'Unet/stage1_test/'
PROCESSED_PATH = 'Unet/stage1_train_processed/'

train_ids = next(os.walk(TRAIN_PATH))[1]
test_ids = next(os.walk(TEST_PATH))[1]

X_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
Y_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool8)
for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
    image_path = f'{PROCESSED_PATH}images/{id_}.png'
    mask_path = f'{PROCESSED_PATH}masks/{id_}.png'
    X_train[n] = gray2rgb(imread(image_path))
    Y_train[n] = np.expand_dims(imread(mask_path), axis=-1)

X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
sizes_test = []

for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
    test_path = f'{PROCESSED_PATH}test/{id_}.png'
    X_test[n] = gray2rgb(imread(test_path))

# load model
model = load_model('model_unet_{}.h5'.format(version.replace('.', '_')))

# summarize model
model.summary()

# Choose a random image index
idx = random.randint(0, len(X_train) - 1)

# Make a prediction in all the 3 sets: training, validation and test.
predictions_for_train_set = model.predict(X_train[:int(X_train.shape[0] * 0.9)], verbose=1)
predictions_for_validation_set = model.predict(X_train[int(X_train.shape[0] * 0.9):], verbose=1)
predictions_for_test_set = model.predict(X_test, verbose=1)

# Convertir los valores por encima de 0.5 en 1
preds_train_t = (predictions_for_train_set > 0.5).astype(np.uint8)
preds_val_t = (predictions_for_validation_set > 0.5).astype(np.uint8)
preds_test_t = (predictions_for_test_set > 0.5).astype(np.uint8)

ix = random.randint(0, len(preds_train_t) - 1)
plt.imshow(X_train[ix])
plt.show()

plt.imshow(np.squeeze(Y_train[ix]))
plt.show()

plt.imshow(np.squeeze(preds_train_t[ix]))
plt.show()

ix = random.randint(0, len(preds_val_t) - 1)
plt.imshow(X_train[int(X_train.shape[0]*0.9):][ix])
plt.show()

plt.imshow(np.squeeze(Y_train[int(Y_train.shape[0]*0.9):][ix]))
plt.show()

plt.imshow(np.squeeze(preds_val_t[ix]))
plt.show()


# for i in range(5):
#     print('%s => %s (expected %s)' % (X_test[i].tolist(), preds_test[i], Y_train[[i]]))

# Evaluate the model
# score = model.evaluate(X_test, Y_test)
# print('{}: Accuracy: {:2f}'.format(model.metrics_names[1], score[1] * 100))
