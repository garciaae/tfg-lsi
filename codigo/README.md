## README
Código para el Trabajo Fin de Grado de Alejandro Manuel García Andrade

### Instrucciones
Para ejecutar el proyecto es recomendable instalar un entorno virtual Python

#### Instalar y activar un entorno virtual python

```
python3 -m venv tfg
. ./tfg/bin/activate
```

#### Instalar los paquetes Python
```
cd dd-ml-segmentation
pip install -r requirements.txt
```

#### Ejecutar el proyecto
```
# Opcional: Hacer login en WandB. Necesitamos darnos de alta en wandb.com
# Este paso solo se hace una vez
wandb login

# Ejecutar el código Python
python main_keras.py
```


#### Notas
Este código está basado parcialmente en el proyecto de DroneDeploy:
https://github.com/dronedeploy/dd-ml-segmentation-benchmark

