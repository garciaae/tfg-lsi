import os

from tensorflow.keras import __version__ as keras_version
from tensorflow.keras.models import load_model
import wandb

from libs import training_keras
from libs import datasets
from libs import inference_keras
from libs import scoring
from libs.models_keras import build_unet
from libs.util_keras import FBeta

GENERATE_CHIPS = False
TRAIN_MODEL = False
WANDB_ENABLED = False
USE_EXPERIMENTAL = False
RUN_INFERENCE = True

if __name__ == '__main__':
    # dataset_name = 'dataset-sample'  # 0.5 GB download
    dataset_name = 'dataset-medium'  # 9.0 GB download

    encoder_name = 'vgg19'
    config = {
        'name': f'baseline-keras-{encoder_name}',
        'dataset': dataset_name,
    }

    if WANDB_ENABLED:
        # If we TRAIN_MODEL, we'll add more stuff to the config
        wandb.init(config=config)

    # Split the images in the dataset to smaller ones (chips) and divide them in training a test set
    if GENERATE_CHIPS:
        datasets.download_dronedeploy_dataset(dataset_name=dataset_name)
        datasets.generate_chips(dataset_name=dataset_name)

    if TRAIN_MODEL:
        model = build_unet(encoder=encoder_name)
        training_keras.train_model(
            dataset_name,
            model,
            encoder_name,
            WANDB_ENABLED,
            USE_EXPERIMENTAL,
        )
    else:
        version = keras_version.replace('.', '_')
        filename = f'model_unet_{version}-{encoder_name}.h5'
        if os.path.exists(filename):
            model = load_model(
                filepath=f'model_unet_{version}-{encoder_name}.h5',
                custom_objects={'FBeta': FBeta},
            )
        else:
            print(f'Couldn\'t open {filename}')
            exit(1)

    basedir = wandb.run.dir if WANDB_ENABLED else 'predictions'
    if RUN_INFERENCE:
        # use the train model to make_chips inference on all test scenes
        inference_keras.run_inference(dataset_name, model=model, basedir=basedir)

    # scores all the test images compared to the ground truth labels
    score, _ = scoring.score_predictions(dataset_name, basedir=basedir)
    print(score)

    # send the scores (f1, precision, recall) and prediction images to wandb
    if WANDB_ENABLED:
        wandb.log(score)
