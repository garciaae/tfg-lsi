import os

from matplotlib import pyplot as plt
from fastai.callbacks import CSVLogger, SaveModelCallback, TrackerCallback
from fastai.callback import Callback
from fastai.metrics import add_metrics
from fastai.torch_core import dataclass, torch, Tensor, Optional, warn
from fastai.basic_train import Learner
from typing import Any
import numpy as np

MASK_COLORS = ["red", "green", "blue", "yellow", "magenta", "cyan"]


class ExportCallback(TrackerCallback):
    """"Exports the model when monitored quantity is best.

    The exported model is the one used for inference.
    """
    def __init__(self, learn:Learner, model_path:str, monitor:str='valid_loss', mode:str='auto'):
        self.model_path = model_path
        super().__init__(learn, monitor=monitor, mode=mode)

    def on_epoch_end(self, epoch:int, **kwargs:Any)->None:
        current = self.get_monitor_value()

        if (epoch == 0 or (current is not None and self.operator(current, self.best))):
            print(f'Better model found at epoch {epoch} with {self.monitor} value: {current} - exporting {self.model_path}')
            self.best = current
            self.learn.export(self.model_path)

# TODO: does this delete some other path or just overwrite?
class MySaveModelCallback(SaveModelCallback):
    """Saves the model after each epoch to potentially resume training.

    Modified from fastai version to delete the previous model that was saved
    to avoid wasting disk space.
    """
    def on_epoch_end(self, epoch:int, **kwargs:Any)->None:
        "Compare the value monitored to its best score and maybe save the model."
        current = self.get_monitor_value()
        if current is not None and self.operator(current, self.best):
            self.best = current
            self.learn.save(f'{self.name}')


class MyCSVLogger(CSVLogger):
    """Logs metrics to a CSV file after each epoch.

    Modified from fastai version to:
    - flush after each epoch
    - append to log if already exists
    """
    def __init__(self, learn, filename='history'):
        super().__init__(learn, filename)

    def on_train_begin(self, **kwargs):
        if self.path.exists():
            # TODO: does this open a file named "a"...?
            self.file = self.path.open('a')
        else:
            super().on_train_begin(**kwargs)

    def on_epoch_end(self, epoch, smooth_loss, last_metrics, **kwargs):
        out = super().on_epoch_end(
            epoch, smooth_loss, last_metrics, **kwargs)
        self.file.flush()
        return out

# The following are a set of metric callbacks that have been modified from the
# original version in fastai to support semantic segmentation, which doesn't
# have the class dimension in position -1. It also adds an ignore_idx
# which is used to ignore pixels with class equal to ignore_idx. These
# would be good to contribute back upstream to fastai -- however we should
# wait for their upcoming refactor of the callback architecture.


@dataclass
class ConfusionMatrix(Callback):
    "Computes the confusion matrix."
    # The index of the dimension in the output and target arrays which ranges
    # over the different classes. This is -1 (the last index) for
    # classification, but is 1 for semantic segmentation.
    clas_idx:int=-1

    def on_train_begin(self, **kwargs):
        self.n_classes = 0


    def on_epoch_begin(self, **kwargs):
        self.cm = None

    def on_batch_end(self, last_output:Tensor, last_target:Tensor, **kwargs):


        preds = last_output.argmax(self.clas_idx).view(-1).cpu()
        targs = last_target.view(-1).cpu()
        if self.n_classes == 0:
            self.n_classes = last_output.shape[self.clas_idx]
            self.x = torch.arange(0, self.n_classes)
        cm = ((preds==self.x[:, None]) & (targs==self.x[:, None, None])).sum(dim=2, dtype=torch.float32)
        if self.cm is None: self.cm =  cm
        else:               self.cm += cm

    def on_epoch_end(self, **kwargs):
        self.metric = self.cm


@dataclass
class CMScores(ConfusionMatrix):
    "Base class for metrics which rely on the calculation of the precision and/or recall score."
    average:Optional[str]="binary"      # `binary`, `micro`, `macro`, `weighted` or None
    pos_label:int=1                     # 0 or 1
    eps:float=1e-9
    # If ground truth label is equal to the ignore_idx, it should be ignored
    # for the sake of evaluation.
    ignore_idx:int=None

    def _recall(self):
        rec = torch.diag(self.cm) / self.cm.sum(dim=1)
        rec[rec != rec] = 0  # removing potential "nan"s
        if self.average is None: return rec
        else:
            if self.average == "micro": weights = self._weights(avg="weighted")
            else: weights = self._weights(avg=self.average)
            return (rec * weights).sum()

    def _precision(self):
        prec = torch.diag(self.cm) / self.cm.sum(dim=0)
        prec[prec != prec] = 0  # removing potential "nan"s
        if self.average is None: return prec
        else:
            weights = self._weights(avg=self.average)
            return (prec * weights).sum()

    def _weights(self, avg:str):
        if self.n_classes != 2 and avg == "binary":
            avg = self.average = "macro"
            warn("average=`binary` was selected for a non binary case. Value for average has now been set to `macro` instead.")
        if avg == "binary":
            if self.pos_label not in (0, 1):
                self.pos_label = 1
                warn("Invalid value for pos_label. It has now been set to 1.")
            if self.pos_label == 1: return Tensor([0,1])
            else: return Tensor([1,0])
        else:
            if avg == "micro": weights = self.cm.sum(dim=0) / self.cm.sum()
            if avg == "macro": weights = torch.ones((self.n_classes,)) / self.n_classes
            if avg == "weighted": weights = self.cm.sum(dim=1) / self.cm.sum()
            if self.ignore_idx is not None and avg in ["macro", "weighted"]:
                weights[self.ignore_idx] = 0
                weights /= weights.sum()
            return weights


class Recall(CMScores):
    """ Compute the Recall. """
    def on_epoch_end(self, last_metrics, **kwargs):
        return add_metrics(last_metrics, self._recall())


class Precision(CMScores):
    """ Compute the Precision. """
    def on_epoch_end(self, last_metrics, **kwargs):
        return add_metrics(last_metrics, self._precision())


@dataclass
class FBeta(CMScores):
    """ Compute the F`beta` score."""
    beta:float=2

    def on_train_begin(self, **kwargs):
        self.n_classes = 0
        self.beta2 = self.beta ** 2
        self.avg = self.average
        if self.average != "micro": self.average = None

    def on_epoch_end(self, last_metrics, **kwargs):
        prec = self._precision()
        rec = self._recall()
        metric = (1 + self.beta2) * prec * rec / (prec * self.beta2 + rec + self.eps)
        metric[metric != metric] = 0  # removing potential "nan"s
        if self.avg: metric = (self._weights(avg=self.avg) * metric).sum()
        return add_metrics(last_metrics, metric)

    def on_train_end(self, **kwargs): self.average = self.avg


def plot_segmentation_history(history, metrics_list=None, losses=None, experiment_title=''):
    """[summary]
    https://github.com/karolzak/keras-unet
    Args:
        history ([type]): [description]
        metrics_list (list, optional): [description]. Defaults to ["miou", "val_miou"].org_imgs
        losses (list, optional): [description]. Defaults to ["loss", "val_loss"].
        experiment_title (str, optional): Title of the experiment
    """
    metrics_list = ("mIOU", "val_mIOU") if not metrics_list else metrics_list
    losses = ("loss", "val_loss") if not losses else losses
    # summarize history for iou Intersection over Union
    basedir = os.path.join(f"{os.getcwd()}/experiments", experiment_title)
    plt.figure(figsize=(12, 6))
    for metric in metrics_list:
        plt.plot(history.history[metric], linewidth=3)
    plt.suptitle("metrics over epochs", fontsize=20)
    plt.ylabel("mIOU", fontsize=20)
    plt.xlabel("epoch", fontsize=20)
    plt.legend(metrics_list, loc="center right", fontsize=15)
    plt.savefig(f"{basedir}/plotted_mIOU.png")

    # summarize history for loss
    plt.figure(figsize=(12, 6))
    for loss in losses:
        plt.plot(history.history[loss], linewidth=3)
    plt.suptitle("loss over epochs", fontsize=20)
    plt.ylabel("loss", fontsize=20)
    plt.xlabel("epoch", fontsize=20)
    plt.legend(losses, loc="center right", fontsize=15)
    print(f"Saving history to {basedir}/plotted_loss.png")
    plt.savefig(f"{basedir}/plotted_loss.png")


def reshape_arr(arr):
    """[summary]

    Args:
        arr (numpy.ndarray): [description]

    Returns:
        numpy.ndarray: [description]
    """
    if arr.ndim == 3:
        return arr
    elif arr.ndim == 4:
        if arr.shape[3] == 3:
            return arr
        elif arr.shape[3] == 1:
            return arr.reshape(arr.shape[0], arr.shape[1], arr.shape[2])


def get_cmap(arr):
    """[summary]

    Args:
        arr (numpy.ndarray): [description]

    Returns:
        string: [description]
    """
    if arr.ndim == 3:
        return "gray"
    elif arr.ndim == 4:
        if arr.shape[3] == 3:
            return "jet"
        elif arr.shape[3] == 1:
            return "gray"


def mask_to_rgba(mask, color="red"):
    """
    Converts binary segmentation mask from white to red color.
    Also adds alpha channel to make black background transparent.

    Args:
        mask (numpy.ndarray): [description]
        color (str, optional): Check `MASK_COLORS` for available colors. Defaults to "red".

    Returns:
        numpy.ndarray: [description]
    """
    assert (color in MASK_COLORS)
    assert (mask.ndim == 3 or mask.ndim == 2)

    h = mask.shape[0]
    w = mask.shape[1]
    zeros = np.zeros((h, w))
    ones = mask.reshape(h, w)
    if color == "red":
        return np.stack((ones, zeros, zeros, ones), axis=-1)
    elif color == "green":
        return np.stack((zeros, ones, zeros, ones), axis=-1)
    elif color == "blue":
        return np.stack((zeros, zeros, ones, ones), axis=-1)
    elif color == "yellow":
        return np.stack((ones, ones, zeros, ones), axis=-1)
    elif color == "magenta":
        return np.stack((ones, zeros, ones, ones), axis=-1)
    elif color == "cyan":
        return np.stack((zeros, ones, ones, ones), axis=-1)


def zero_pad_mask(mask, desired_size):
    """[summary]

    Args:
        mask (numpy.ndarray): [description]
        desired_size ([type]): [description]

    Returns:
        numpy.ndarray: [description]
    """
    pad = (desired_size - mask.shape[0]) // 2
    padded_mask = np.pad(mask, pad, mode="constant")
    return padded_mask


def plot_imgs(org_imgs, mask_imgs, pred_imgs=None, nm_img_to_plot=10, figsize=4, alpha=0.5,
              color="red"):
    """
    Image plotting for semantic segmentation data.
    Last column is always an overlay of ground truth or prediction
    depending on what was provided as arguments.
    Args:
        org_imgs (numpy.ndarray): Array of arrays representing a collection of original images.
        mask_imgs (numpy.ndarray): Array of arrays representing a collection of mask images (grayscale).
        pred_imgs (numpy.ndarray, optional): Array of arrays representing a collection of prediction masks images.. Defaults to None.
        nm_img_to_plot (int, optional): How many images to display. Takes first N images. Defaults to 10.
        figsize (int, optional): Matplotlib figsize. Defaults to 4.
        alpha (float, optional): Transparency for mask overlay on original image. Defaults to 0.5.
        color (str, optional): Color for mask overlay. Defaults to "red".
    """  # NOQA E501
    assert(color in MASK_COLORS)

    if nm_img_to_plot > org_imgs.shape[0]:
        nm_img_to_plot = org_imgs.shape[0]
    im_id = 0
    org_imgs_size = org_imgs.shape[1]

    org_imgs = reshape_arr(org_imgs)
    mask_imgs = reshape_arr(mask_imgs)
    if not (pred_imgs is None):
        cols = 4
        pred_imgs = reshape_arr(pred_imgs)
    else:
        cols = 3

    fig, axes = plt.subplots(
        nm_img_to_plot, cols, figsize=(cols * figsize, nm_img_to_plot * figsize), squeeze=False
    )
    axes[0, 0].set_title("original", fontsize=15)
    axes[0, 1].set_title("ground truth", fontsize=15)
    if not (pred_imgs is None):
        axes[0, 2].set_title("prediction", fontsize=15)
        axes[0, 3].set_title("overlay", fontsize=15)
    else:
        axes[0, 2].set_title("overlay", fontsize=15)
    for m in range(0, nm_img_to_plot):
        axes[m, 0].imshow(org_imgs[im_id], cmap=get_cmap(org_imgs))
        axes[m, 0].set_axis_off()
        axes[m, 1].imshow(mask_imgs[im_id], cmap=get_cmap(mask_imgs))
        axes[m, 1].set_axis_off()
        if not (pred_imgs is None):
            axes[m, 2].imshow(pred_imgs[im_id], cmap=get_cmap(pred_imgs))
            axes[m, 2].set_axis_off()
            axes[m, 3].imshow(org_imgs[im_id], cmap=get_cmap(org_imgs))
            axes[m, 3].imshow(
                mask_to_rgba(
                    zero_pad_mask(pred_imgs[im_id], desired_size=org_imgs_size),
                    color=color,
                ),
                cmap=get_cmap(pred_imgs),
                alpha=alpha,
            )
            axes[m, 3].set_axis_off()
        else:
            axes[m, 2].imshow(org_imgs[im_id], cmap=get_cmap(org_imgs))
            axes[m, 2].imshow(
                mask_to_rgba(
                    zero_pad_mask(mask_imgs[im_id], desired_size=org_imgs_size),
                    color=color,
                ),
                cmap=get_cmap(mask_imgs),
                alpha=alpha,
            )
            axes[m, 2].set_axis_off()
        im_id += 1

    plt.show()
