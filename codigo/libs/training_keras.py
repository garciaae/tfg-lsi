from datetime import datetime

from tensorflow.keras import metrics
from tensorflow.keras import __version__ as keras_version
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import History
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import TensorBoard
import wandb
from wandb.keras import WandbCallback

from libs.datasets_keras import enable_dynamic_memory_growth
from libs.datasets_keras import load_dataset
from libs.util_keras import FBeta
from libs.util import plot_segmentation_history


def train_model(dataset, model, encoder='', wandb_enabled=False, use_experimental=False):
    now = datetime.now()
    experiment_title = f"{now.strftime('%Y-%m-%d_%H-%M-%S')}-{encoder}"
    config = {
        'epochs': 15,
        'lr': 1e-4,
        'size': 300,
        'wd': 1e-2,
        'bs': 8,  # reduce this if you are running out of GPU memory
        'pretrained': True,
        'title': experiment_title,
    }

    if use_experimental:
        enable_dynamic_memory_growth()

    model.compile(
        optimizer=Adam(lr=config['lr']),
        loss='categorical_crossentropy',
        metrics=[
            metrics.Precision(top_k=1, name='precision'),
            metrics.Recall(top_k=1, name='recall'),
            FBeta(name='f_beta')
        ]
    )

    train_data, valid_data = load_dataset(dataset, config['bs'])
    _, ex_data = load_dataset(dataset, 10)

    # Callbacks
    checkpointer = ModelCheckpoint(
        'keras_intermediate.h5',
        verbose=1,
        save_freq='epoch',
        save_best_only=True,
    )
    history = History()
    callbacks = [
        checkpointer,
        EarlyStopping(patience=2, monitor='val_loss'),
        history,
        TensorBoard(log_dir='logs'),
    ]
    if wandb_enabled:
        wandb.config.update(config)
        wandb_callback = WandbCallback(
            input_type='image',
            output_type='segmentation_mask',
            validation_data=ex_data[0]
        )
        callbacks.append(wandb_callback)

    model.fit(
        train_data,
        validation_data=valid_data,
        epochs=config['epochs'],
        callbacks=callbacks,
    )

    # Guardar el modelo
    version = keras_version.replace('.', '_')
    model.save(f'model_unet_{version}-{encoder}.h5')

    # History of the model
    print(f"Saving the experiment history {experiment_title}")
    plot_segmentation_history(history, experiment_title)
