import logging
from shutil import rmtree

import os
import sys

from libs.images2chips import make_chips


logger = logging.getLogger(__name__)


def download_dronedeploy_dataset(dataset_name):
    """ Download a dataset, extract it and create the tiles """
    datasets = {
        'dataset-sample': 'https://dl.dropboxusercontent.com/s/h8a8kev0rktf4kq/dataset-sample.tar.gz?dl=0',
        'dataset-medium': 'https://dl.dropboxusercontent.com/s/r0dj9mhyv4bgbme/dataset-medium.tar.gz?dl=0',
    }

    # The smaller dataset consists in 3 images:
    # 1d4fbe33f3_F1BE1D4184INSPIRE-ortho
    # e1d3e6f6ba_B4DE0FB544INSPIRE-ortho
    # ec09336a6f_06BA0AF311OPENPIPELINE-ortho

    if dataset_name not in datasets:
        logger.error("Unknown dronedeploy dataset %s", dataset_name)
        sys.exit(0)

    filename = f'{dataset_name}.tar.gz'
    if os.path.exists(filename):
        print(f'compressed "{filename}" already existed, removing it')
        os.remove(filename)

    logger.info('downloading dataset %s', dataset_name)
    url = datasets[dataset_name]
    os.system(f'curl "{url}" -o {filename}')

    if os.path.exists(dataset_name):
        logger.warning(f'folder {dataset_name} already exists, removing it to re-create.')
        rmtree(dataset_name)

    logger.info(f'extracting %s', filename)
    os.system(f'tar -xvf {filename}')


def generate_chips(dataset_name):
    image_chips = f'{dataset_name}/image-chips'
    label_chips = f'{dataset_name}/label-chips'
    eleva_chips = f'{dataset_name}/eleva-chips'

    if os.path.exists(image_chips) or os.path.exists(label_chips):
        logger.info('chip and label folders already exist: removing them to recreate chips.')
        rmtree(image_chips)
        rmtree(label_chips)
        rmtree(eleva_chips)
    logger.info("creating chips")
    make_chips(dataset_name)
