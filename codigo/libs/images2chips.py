import os
from itertools import repeat

from multiprocessing import Pool

import cv2
import numpy as np

from libs.config import val_ids, test_ids, LABELMAP, INV_LABELMAP


def color2class(img):
    ret = np.zeros((img.shape[0], img.shape[1]), dtype='uint8')
    ret = np.dstack([ret, ret, ret])
    colors = np.unique(img.reshape(-1, img.shape[2]), axis=0)

    # Skip any chips that would contain magenta (IGNORE) pixels
    ignore_color = LABELMAP[0]  # Ignore magenta
    for color in colors:
        if tuple(color) == ignore_color:
            return None

        locs = np.where(
            (img[:, :, 0] == color[0]) & (img[:, :, 1] == color[1]) & (img[:, :, 2] == color[2])
        )
        ret[locs[0], locs[1], :] = INV_LABELMAP[tuple(color)] - 1

    return ret


def image2tile(dataset_name, scene, dataset, orthofile, elevafile, labelfile, tile_width=300,
               tile_height=300, stridex=300, stridey=300):
    """ Final step in chips generation write the chips in disk"""
    ortho = cv2.imread(orthofile)
    label = cv2.imread(labelfile)

    # Not using elevation in the sample - but useful to incorporate it ;)
    # Loads image as such including alpha channel
    eleva = cv2.imread(elevafile, cv2.IMREAD_UNCHANGED)

    # Both image and ortho should have the same dimensions
    assert(ortho.shape[0] == label.shape[0])
    assert(ortho.shape[1] == label.shape[1])

    shape = ortho.shape

    xsize = shape[1]  # width
    ysize = shape[0]  # height
    print(
        f"converting {dataset} image {orthofile} {xsize}x{ysize} to {tile_width}x{tile_height} "
        f"chips ..."
    )

    counter = 0

    for xi in range(0, xsize - tile_width, stridex):
        for yi in range(0, ysize - tile_height, stridey):
            orthochip = ortho[yi:yi + tile_height, xi:xi + tile_width, :]
            labelchip = label[yi:yi + tile_height, xi:xi + tile_width, :]
            elevachip = eleva[yi:yi + tile_height, xi:xi + tile_width]

            classchip = color2class(labelchip)

            if classchip is None:
                continue

            orthochip_filename = os.path.join(
                dataset_name,
                'image-chips',
                scene + '-' + str(counter).zfill(6) + '.png',
            )
            print(f"Chip {orthochip_filename} from {yi}:{yi+tile_height}, {xi}:{xi+tile_width}")
            labelchip_filename = os.path.join(
                dataset_name,
                'label-chips',
                scene + '-' + str(counter).zfill(6) + '.png',
            )
            elevachip_filename = os.path.join(
                dataset_name,
                'eleva-chips',
                scene + '-' + str(counter).zfill(6) + '.png',
            )

            with open(f"{dataset_name}/{dataset}", mode='a') as fd:
                fd.write(scene + '-' + str(counter).zfill(6) + '.png\n')

            cv2.imwrite(orthochip_filename, orthochip)
            cv2.imwrite(labelchip_filename, classchip)
            cv2.imwrite(elevachip_filename, elevachip)
            counter += 1


def get_split(scene):
    if scene in val_ids:
        return 'valid.txt'
    if scene in test_ids:
        return 'test.txt'
    return "train.txt"


def make_chips(dataset_name):
    """ Make smaller images out of the given ones."""
    # Create 3 new files for training, validation and testing
    for file_name in ('train', 'valid', 'test'):
        open(f'{dataset_name}/{file_name}.txt', mode='w').close()

    # Create the dirs for image and labels
    for label in ('image-chips', 'label-chips', 'eleva-chips'):
        if not os.path.exists(os.path.join(dataset_name, label)):
            os.mkdir(os.path.join(dataset_name, label))

    lines = [line for line in open(f'{dataset_name}/index.csv')]
    num_images = len(lines) - 1
    print(
        f"converting {num_images} images to chips - "
        f"this may take a few minutes but only needs to be done once."
    )
    # Convert the images to chips in parallel
    pool = Pool(processes=8)
    pool.starmap(convert_image, zip(lines, repeat(dataset_name)))


def convert_image(line, dataset_name):
    line = line.strip().split(' ')
    file_name = line[1]  # The second field contains the file name
    dataset = get_split(file_name)

    if dataset == 'test.txt':
        print(f"not converting test image {file_name} to chips, it will be used for inference.")
        return

    orthofile = os.path.join(dataset_name, 'images', file_name + '-ortho.tif')
    elevafile = os.path.join(dataset_name, 'elevations', file_name + '-elev.tif')
    labelfile = os.path.join(dataset_name, 'labels', file_name + '-label.png')

    if os.path.exists(orthofile) and os.path.exists(labelfile):
        image2tile(dataset_name, file_name, dataset, orthofile, elevafile, labelfile)
